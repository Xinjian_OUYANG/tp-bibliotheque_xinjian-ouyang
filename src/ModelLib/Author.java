package ModelLib;

public class Author {
    private String nameAuthor;
    private int birthyear;

    public String getNameAuthor() {
        return nameAuthor;
    }
    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }
    public int getBirthyear() {
        return birthyear;
    }
    public void setBirthyear(int birthyear) {
        this.birthyear = birthyear;
    }


    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Author other = (Author) obj;
        if (nameAuthor == null) {
            if (other.nameAuthor != null)
                return false;
        } else if (!nameAuthor.equals(other.nameAuthor))
            return false;
        if (birthyear!= other.birthyear)
            return false;
        return true;
    }


    public String toString() {
        return "Author [nameAuthor=" + nameAuthor + ", birthyear=" + birthyear + "]";
    }


}
