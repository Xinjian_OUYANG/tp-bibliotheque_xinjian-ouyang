package ModelLib;

public class Librarian {
    private String nameLibrarian;
    private String passwordLibrarian;
    public String getNameLibrarian() {
        return nameLibrarian;
    }
    public void setNameLibrarian(String nameLibrarian) {

        this.nameLibrarian = nameLibrarian;
    }
    public String getPasswordLibrarian() {

        return passwordLibrarian;
    }
    public void setPasswordLibrarian(String passwordLibrarian) {

        this.passwordLibrarian = passwordLibrarian;
    }


    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Librarian other = (Librarian) obj;
        if (nameLibrarian == null) {
            return other.nameLibrarian == null;
        } else
            return nameLibrarian.equals(other.nameLibrarian);
    }

    public String toString() {
        return "Librarian [nameLibrarian=" + nameLibrarian + ", passwordLibrarian=" + passwordLibrarian + "]";
    }

}
