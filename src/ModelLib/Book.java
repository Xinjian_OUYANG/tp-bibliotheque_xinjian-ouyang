package ModelLib;

public class Book {

    private int idBook;
    private String nameBook;
    private String kind;
    private String nameAuthor;
    private String namePublisher;

    public int getIdBook() {
        return idBook;
    }
    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }
    public String getNameBook() {
        return nameBook;
    }
    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }
    public String getKind() {
        return kind;
    }
    public void setKind(String kind) {
        this.kind = kind;
    }
    public String getNameAuthor() {
        return nameAuthor;
    }
    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }
    public String getNamePublisher() {
        return namePublisher;
    }
    public void setNamePublisher(String namePublisher) {
        this.namePublisher = namePublisher;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Book other = (Book) obj;
        if (nameAuthor == null) {
            if (other.nameAuthor != null)
                return false;
        } else if (!nameAuthor.equals(other.nameAuthor))
            return false;
        if (idBook == ' ') {
            return false;
        } else if (idBook != other.idBook)
            return false;
        if (nameBook == null) {
            if (other.nameBook != null)
                return false;
        } else if (!nameBook.equals(other.nameBook))
            return false;
        if (namePublisher == null) {
            if (other.namePublisher != null)
                return false;
        } else if (!namePublisher.equals(other.namePublisher))
            return false;
        if (kind == null) {
            return other.kind == null;
        } else return kind.equals(other.kind);
    }

    public String toString() {
        return "Book [idBook=" + idBook + ", nameBook=" + nameBook + ", kind=" + kind + ", nameAuthor="
                + nameAuthor + ", namePublisher=" + namePublisher + "]";
    }

}
