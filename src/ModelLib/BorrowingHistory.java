package ModelLib;

public class BorrowingHistory {
    private int idReader;
    private int idBook;
    private String nameBook;
    private String borrowDate;
    private String returnDate;
    private Boolean isReturned;

    public int getIdReader() {return idReader; }
    public void setIdReader(int idReader) {
        this.idReader = idReader;
    }
    public int getIdBook() {
        return idBook;
    }
    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }
    public String getNameBook() {
        return nameBook;
    }
    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }
    public String getBorrowDate() {
        return borrowDate;
    }
    public void setBorrowDate(String borrowDate) {
        this.borrowDate = borrowDate;
    }
    public String getReturnDate() {
        return returnDate;
    }
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }
    public Boolean getIsReturned(){return isReturned;}
    public void setIsReturned(Boolean isReturned){this.isReturned = isReturned;}
}
