package ModelLib;

public class Reader {
    private int idReader;
    private String nameReader;
    private String emailReader;
    private String passwordReader;
    public int getIdReader() {
        return idReader;
    }
    public void setIdReader(int idReader) {
        this.idReader = idReader;
    }
    public String getNameReader() {
        return nameReader;
    }
    public void setNameReader(String nameReader) {
        this.nameReader = nameReader;
    }
    public String getEmailReader() {
        return emailReader;
    }
    public void setEmailReader(String emailReader) {
        this.emailReader = emailReader;
    }
    public String getPasswordReader() {
        return passwordReader;
    }
    public void setPasswordReader(String passwordReader) {
        this.passwordReader = passwordReader;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Reader other = (Reader) obj;
        if (idReader == ' ') {
            if (other.idReader != ' ')
                return false;
        } else if (idReader!=other.idReader)
            return false;
        if (nameReader == null) {
            if (other.nameReader != null)
                return false;
        } else if (!nameReader.equals(other.nameReader))
            return false;
        if (emailReader == null) {
            return other.emailReader == null;
        } else if (!emailReader.equals(other.emailReader))
            return false;
        return true;
    }

    public String toString() {
        return "Reader [idReader=" + idReader + ", nameReader=" + nameReader +  ", emailReader=" + emailReader
                + ", passwordReader=" + passwordReader + "]";
    }

}
