package ModelLib;

public class Blacklist {
    private int idReaderBlack;
    private String nameReaderBlack;

    public int getIdReaderBlack() {
        return idReaderBlack;
    }
    public void setIdReaderBlack(int idReaderBlack) {
        this.idReaderBlack = idReaderBlack;
    }
    public String getNameReaderBlack(){ return nameReaderBlack;}
    public void setNameReaderBlack(String nameReaderBlack){this.nameReaderBlack = nameReaderBlack;}


    public String toString() {
        return "Blacklist [Reader Id =" + idReaderBlack ;
    }




}
