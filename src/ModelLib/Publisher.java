package ModelLib;

// add the information of a new book
public class Publisher {

    private String namePublisher;
    private int pubyear;

    public String getNamePublisher() {
        return namePublisher;
    }
    public void setNamePublisher(String namePublisher) {
        this.namePublisher = namePublisher;
    }
    public int getPubyear() {
        return pubyear;
    }
    public void setPubyear(int pubyear) {
        this.pubyear = pubyear;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Publisher other = (Publisher) obj;
        if (pubyear != other.pubyear) {
            return false;
        }
        if (namePublisher == null) {
            return other.namePublisher == null;
        } else return namePublisher.equals(other.namePublisher);
    }

    public String toString() {
        return "Publisher [namePublisher=" + namePublisher + ", publication year=" + pubyear + "]";
    }

}



