package ModelLib;


public class Borrow {
    private int idReader;
    private int idBook;
    private String borrowDate;
    private String dueDate;
    private Boolean overtime;
    public int getIdReader() {
        return idReader;
    }
    public void setIdReader(int idReader) {
        this.idReader = idReader;
    }
    public int getIdBook() {
        return idBook;
    }
    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }
    public String getBorrowDate() {
        return borrowDate;
    }
    public void setBorrowDate(String borrowDate) {
        this.borrowDate = borrowDate;
    }
    public String getDueDate() {
        return dueDate;
    }
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    public Boolean getOvertime() {
        return overtime;
    }
    public void setOvertime(Boolean overtime) {
        this.overtime = overtime;
    }

    public String toString() {
        return "Borrow [idReader=" + idReader + ", idBook=" + idBook + ", borrowDate=" + borrowDate + ", dueDate=" + dueDate
                + ", overtime=" + overtime + "]";
    }

}
