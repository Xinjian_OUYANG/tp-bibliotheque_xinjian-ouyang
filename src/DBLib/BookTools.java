package DBLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ModelLib.Book;

public class BookTools {

    //all books' data
    public List<Book> BookData() {
        String sql="select idBook,nameBook,kind,nameAuthor,namePublisher from Book";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Book> ls=new ArrayList<Book>();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                Book book=new Book();
                book.setIdBook(rs.getInt("idBook"));
                book.setNameBook(rs.getString("nameBook"));
                book.setKind(rs.getString("kind"));
                book.setNameAuthor(rs.getString("nameAuthor"));
                book.setNamePublisher(rs.getString("namePublisher"));
                ls.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    //show a specific book's data by the keyword of its name
    public List<Book> BookData(String nameBook) {
        String sql="select idBook,nameBook,kind,nameAuthor,namePublisher from Book where nameBook like'%"+ nameBook +"%'";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Book> ls=new ArrayList<Book>();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                Book book=new Book();
                book.setIdBook(rs.getInt("idBook"));
                book.setNameBook(rs.getString("nameBook"));
                book.setKind(rs.getString("kind"));
                book.setNameAuthor(rs.getString("nameAuthor"));
                book.setNamePublisher(rs.getString("namePublisher"));
                ls.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    //Search a book by its ID
    public static Book Search_Book(int idBook) {
        String sql="select idBook,nameBook,kind,nameAuthor,namePublisher from Book where idBook=" + idBook;
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        Book book = null;
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                book=new Book();
                book.setIdBook(rs.getInt("idBook"));
                book.setNameBook(rs.getString("nameBook"));
                book.setKind(rs.getString("kind"));
                book.setNameAuthor(rs.getString("nameAuthor"));
                book.setNamePublisher(rs.getString("namePublisher"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return book;
    }

    //add a book
    public int AddBook(Book book) {
        int i=0;
        String sql="insert into book (idBook,nameBook,kind,nameAuthor,namePublisher)values(?,?,?,?,?)";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, book.getIdBook());
            st.setString(2, book.getNameBook());
            st.setString(3, book.getKind());
            st.setString(4, book.getNameAuthor());
            st.setString(5, book.getNamePublisher());
            i=st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    //update a book's data
    public int UpdateBook(Book book) {
        int i=0;
        String sql="update book set idBook=?,nameBook=?,kind=?,nameAuthor=?,namePublisher=? where idBook=?";
        DBConn db = new DBConn();
        PreparedStatement st = null;
        Connection conn = db.getConn();
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, book.getIdBook());
            st.setString(2, book.getNameBook());
            st.setString(3, book.getKind());
            st.setString(4, book.getNameAuthor());
            st.setString(5, book.getNamePublisher());
            st.setInt(6, book.getIdBook());
            i=st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    //delete a specific book by its ID
    public int DeleteBook(int idbook) {
        int i=0;
        String sql="delete from Book where idBook=?";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, idbook);
            i=st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

}

