package DBLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ModelLib.Publisher;

public class PublisherTools {

    //return specific publisher according to its name
    public Publisher PublisherData(String namePublisher) {
        String sql="select namePublisher,pubyear from publisher where namePublisher='" + namePublisher + "'";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        Publisher publisher = new Publisher();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                publisher.setNamePublisher(rs.getString("namePublisher"));
                publisher.setPubyear(rs.getInt("pubyear"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return publisher;
    }

    public List<Publisher> PublisherData() {
        String sql="select namePublisher,pubyear from publisher";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Publisher> ls= new ArrayList<>();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                Publisher publisher=new Publisher();
                publisher.setNamePublisher(rs.getString("namePublisher"));
                publisher.setPubyear(rs.getInt("pubyear"));
                ls.add(publisher);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    public int AddPublisher(Publisher publisher) {
        int i=0;
        String sql="insert into publisher (namePublisher,pubyear)values(?,?)";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setString(1, publisher.getNamePublisher());
            st.setInt(2, publisher.getPubyear());
            i=st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    public int UpdatePublisher(Publisher publisher) {
        int i=0;
        String sql="update publisher set namePublisher=?,pubyear=? where namePublisher=?";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setString(1, publisher.getNamePublisher());
            st.setInt(2, publisher.getPubyear());
            st.setString(3, publisher.getNamePublisher());
            i=st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }
}

