package DBLib;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import ModelLib.Blacklist;
import ModelLib.Reader;

public class BlacklistTools {
    public List<Blacklist> BlacklistData(int idReader) {
        String sql = "select idReaderblack,nameReaderblack from blacklist where idReaderblack =" + idReader;
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Blacklist> ls = new ArrayList<>();
        try {
            st =conn.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Blacklist blackuser = new Blacklist();
                blackuser.setIdReaderBlack(rs.getInt("idReaderblack"));
                blackuser.setNameReaderBlack(rs.getString("nameReaderblack"));
                ls.add(blackuser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    //fuzzy search 模糊搜索
    public List<Blacklist> BlackuserSearch(String nameReader) {
        String sql = "select idReaderblack,nameReaderblack from blacklist where nameReaderblack like'%" + nameReader + "%'";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Blacklist> ls = new ArrayList<>();
        try {
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Blacklist blackuser = new Blacklist();
                blackuser.setIdReaderBlack(rs.getInt("idReaderblack"));
                blackuser.setNameReaderBlack(rs.getString("nameReaderblack"));
                ls.add(blackuser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }


    public List<Blacklist> BlacklistData() {
        String sql = "select idReaderblack,nameReaderblack from blacklist";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Blacklist> ls = new ArrayList<>();
        try {
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Blacklist blackuser = new Blacklist();
                blackuser.setIdReaderBlack(rs.getInt("idReaderblack"));
                blackuser.setNameReaderBlack(rs.getString("nameReaderblack"));
                ls.add(blackuser);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    public int addBlackuser(Blacklist blackuser) {
        int i = 0;
        String sql = "insert into blacklist (idReaderblack,nameReaderblack)values(?,?)";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, blackuser.getIdReaderBlack());
            st.setString(2, blackuser.getNameReaderBlack());
            i = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //System.out.println(i);
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    public int DeleteBlackuser(int idreader) {
        int i = 0;
        String sql = "delete from blacklist where idReaderblack=?";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, idreader);
            i = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

}
