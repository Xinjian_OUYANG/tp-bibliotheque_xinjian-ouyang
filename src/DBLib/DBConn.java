package DBLib;

import java.sql.*;

//JDBC: Java Data Base Connectivity
public class DBConn {
    public Connection conn = null;
    public Connection getConn() {
        try {
            //db parameters
            String DBurl= "jdbc:sqlite:E:\\CS learning\\ST5\\Capteurs et data base\\Project_Library_Xinjian\\DBlibrary.db";
            //create a connection to database
            conn = DriverManager.getConnection(DBurl);
            System.out.println("Connect successfully.");
        }
        //exceptional case 抛出异常
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }

}

