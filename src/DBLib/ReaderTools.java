package DBLib;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import ModelLib.Reader;

public class ReaderTools {

    public boolean isInBlacklist(int idReader){
        String sql = "select idReaderblack from blacklist";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        try{
            st =conn.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                if (idReader == rs.getInt("idReaderblack")) {
                    return true;
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public List<Reader> ReaderData(int idReader) {
        String sql = "select idReader,nameReader,emailReader,password from reader where idReader =" + idReader;
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Reader> ls = new ArrayList<>();
        try {
            st =conn.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Reader reader = new Reader();
                reader.setIdReader(rs.getInt("idReader"));
                reader.setNameReader(rs.getString("nameReader"));
                reader.setEmailReader(rs.getString("emailReader"));
                reader.setPasswordReader(rs.getString("password"));
                ls.add(reader);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    //fuzzy search 模糊搜索
    public List<Reader> ReaderDataSearch(String nameReader) {
        String sql = "select idReader,nameReader,emailReader,password from reader where nameReader like'%" + nameReader + "%'";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Reader> ls = new ArrayList<>();
        try {
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Reader reader = new Reader();
                reader.setIdReader(rs.getInt("idReader"));
                reader.setNameReader(rs.getString("nameReader"));
                reader.setEmailReader(rs.getString("emailReader"));
                reader.setPasswordReader(rs.getString("password"));
                ls.add(reader);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }


    public List<Reader> ReaderData() {
        String sql = "select idReader,nameReader,emailReader,password from reader";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Reader> ls = new ArrayList<>();
        try {
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Reader reader = new Reader();
                reader.setIdReader(rs.getInt("idReader"));
                reader.setNameReader(rs.getString("nameReader"));
                reader.setEmailReader(rs.getString("emailReader"));
                reader.setPasswordReader(rs.getString("password"));
                ls.add(reader);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    public boolean ReaderLogin(int idReader, String password) {
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            String sql = "select idReader,password from reader where idReader=" + idReader + " and password='"
                    + password + "'";
            st = conn.prepareStatement(sql);
            rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public int AddReader(Reader reader) {
        int i = 0;
        String sql = "insert into reader (idReader,nameReader,emailReader,password)values(?,?,?,?)";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, reader.getIdReader());
            st.setString(2, reader.getNameReader());
            st.setString(3, reader.getEmailReader());
            st.setString(4, reader.getPasswordReader());
            i = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    public int UpdateReader(Reader reader) {
        int i = 0;
        String sql = "update reader set idReader=?,nameReader=?,emailReader=?,password=? where idReader=?";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, reader.getIdReader());
            st.setString(2, reader.getNameReader());
            st.setString(3, reader.getEmailReader());
            st.setString(4, reader.getPasswordReader());
            st.setInt(5, reader.getIdReader());
            i = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    public int DeleteReader(int idreader) {
        int i = 0;
        String sql = "delete from reader where idReader=?";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, idreader);
            i = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }
}

