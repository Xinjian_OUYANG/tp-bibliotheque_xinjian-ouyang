package DBLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LibrarianTools{

    public boolean LibrarianLogin(String nameLibrarian, String password) {
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        try {
            String sql = "select nameLibrarian,password from librarian where nameLibrarian='" + nameLibrarian + "' and password='" + password+"'";
            st =conn.prepareStatement(sql);
            rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}

