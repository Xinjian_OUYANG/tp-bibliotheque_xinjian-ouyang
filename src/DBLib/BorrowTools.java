package DBLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ModelLib.Book;
import ModelLib.BorrowingHistory;

public class BorrowTools {

    public List<Book> BookData(int idReader) {
        String sql="select book.idBook,nameBook,book.kind,nameAuthor,namePublisher from reader,borrow,book "
                + "where book.idBook = borrow.idBook and reader.idReader = borrow.idReader "
                + "and reader.idReader = " + idReader ;
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Book> ls=new ArrayList<Book>();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                Book book=new Book();
                book.setIdBook(rs.getInt("idBook"));
                book.setNameBook(rs.getString("nameBook"));
                book.setKind(rs.getString("kind"));
                book.setNameAuthor(rs.getString("nameAuthor"));
                book.setNamePublisher(rs.getString("namePublisher"));
                ls.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    //equalts to BookTools.Search_Book
    public List<Book> BookData_Search_idBook(int idBook) {
        String sql="select book.idBook,nameBook,book.kind,nameAuthor,namePublisher from book where book.idBook = '" + idBook + "'";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Book> ls=new ArrayList<Book>();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                Book book=new Book();
                book.setIdBook(rs.getInt("idBook"));
                book.setNameBook(rs.getString("nameBook"));
                book.setKind(rs.getString("kind"));
                book.setNameAuthor(rs.getString("nameAuthor"));
                book.setNamePublisher(rs.getString("namePublisher"));
                ls.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    public boolean whetherInStock(int idBook) {
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        String sql = "select * from borrow ";
        PreparedStatement st = null;
        try {
            st =conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if(rs.getInt("idBook")!=' '){
                    if(rs.getInt("idBook") == (idBook)){
                        return false;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public int BorrowBook(int idReader,int idBook) {
        int i=0;
        String sql="insert into borrow (idReader,idBook,borrowDate,dueDate,overtime)values(?,?,datetime('now'),datetime('now', '+2 months'),0)";
        DBConn db = new DBConn();
        PreparedStatement st = null;
        Connection conn = db.getConn();
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, idReader);
            st.setInt(2, idBook);
            i = st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    public int BorrowHistory(int idReader,int idBook,String nameBook) {
        int i=0;
        String sql="insert into borrowingHistory (idReader,idBook,nameBook,borrowDate,isReturned)values(?,?,?,datetime('now'),0)";
        DBConn db = new DBConn();
        PreparedStatement st = null;
        Connection conn = db.getConn();
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, idReader);
            st.setInt(2, idBook);
            st.setString(3,nameBook);
            i = st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;

    }
    public List<BorrowingHistory> HistoryData(int idReader) {
        String sql="select idBook, nameBook, borrowDate, returnDate, isReturned from borrowingHistory  where idReader =" + idReader ;
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<BorrowingHistory> ls=new ArrayList<>();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                BorrowingHistory book = new BorrowingHistory();
                book.setIdReader(idReader);
                book.setIdBook(rs.getInt("idBook"));
                book.setNameBook(rs.getString("nameBook"));
                book.setBorrowDate(rs.getString("borrowDate"));
                book.setReturnDate(rs.getString("returnDate"));
                book.setIsReturned(rs.getBoolean("isReturned"));
                ls.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    public int ReturnBook(int idBook) {
        int i = 0;
        String sql1 = "delete from borrow where idBook=?";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st1 = null;
        try {
            st1 = conn.prepareStatement(sql1);
            st1.setInt(1, idBook);
            i = st1.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (st1 != null) {
                try {
                    st1.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    public int ReturnHis(int idBook) {
        int i = 0;
        String sql = "update borrowingHistory set isReturned=? ,returnDate=datetime('now') where idBook=?";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setInt(1, 1);
            st.setInt(2,idBook);
            i = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }



}
