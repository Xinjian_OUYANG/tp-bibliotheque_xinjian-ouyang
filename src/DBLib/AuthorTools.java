package DBLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ModelLib.Author;

public class AuthorTools {

    //show a specific author's data
    public List<Author> AuthorData(String nameAuthor) {
        //define a sql command
        String sql="select nameAuthor,birthyear from author where nameAuthor='" + nameAuthor + "'";
        DBConn db = new DBConn();
        Connection conn = db.getConn();

        ResultSet rs = null;
        PreparedStatement st = null;

        List<Author> ls= new ArrayList<>();
        try {
            st =conn.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                Author author=new Author();
                author.setNameAuthor(rs.getString("nameAuthor"));
                author.setBirthyear(rs.getInt("birthyear"));
                ls.add(author);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
         return ls;
    }

    //show all authors' data
    public List<Author> AuthorData() {
        String sql="select nameAuthor,birthyear from author";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        ResultSet rs = null;
        PreparedStatement st = null;
        List<Author> ls= new ArrayList<>();
        try {
            st =conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                Author author=new Author();
                author.setNameAuthor(rs.getString("nameAuthor"));
                author.setBirthyear(rs.getInt("birthday"));
                ls.add(author);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ls;
    }

    //add a new author
    public int AddAuthor(Author author) {
        int i=0;
        String sql="insert into author (nameAuthor,birthyear)values(?,?)";
        DBConn db = new DBConn();
        Connection conn = db.getConn();
        PreparedStatement st = null;
        try {
            st = conn.prepareStatement(sql);
            st.setString(1, author.getNameAuthor());
            st.setInt(2, author.getBirthyear());
            i = st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    //update a author's data
    public int UpdateAuthor(Author author) {
        int i=0;
        String sql="update author set nameAuthor=?,birthyear=? where nameAuthor=?";
        DBConn db = new DBConn();
        PreparedStatement st = null;
        Connection conn = db.getConn();
        try {
            st = conn.prepareStatement(sql);
            st.setString(1, author.getNameAuthor());
            st.setInt(2, author.getBirthyear());
            st.setString(3, author.getNameAuthor());
            i = st.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

}

