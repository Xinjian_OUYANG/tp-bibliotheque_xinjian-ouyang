import GuiLib.LogInFrame;

import javax.swing.*;
import java.awt.EventQueue;

public class MainGUI {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                JFrame frame = new LogInFrame();
                frame.setVisible(true);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
