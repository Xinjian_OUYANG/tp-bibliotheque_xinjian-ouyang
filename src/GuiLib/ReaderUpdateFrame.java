package GuiLib;

import ModelLib.Reader;
import DBLib.ReaderTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ReaderUpdateFrame extends JFrame {

	private JPanel contentPane;
	
	private JTextField nameReadertextField;
	private JTextField emailtextField;
	private JTextField passwordtextField;

	private JLabel idReader_showLabel;
	private JLabel idReaderLabel;
	private JLabel nameReaderLabel;
	private JLabel emailLabel;
	private JLabel passwordLabel;
	private JButton updateButton;

	public ReaderUpdateFrame(allReadersFrame allReadersFrame) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		idReaderLabel = new JLabel("Reader's ID");
		idReaderLabel.setBounds(100, 100, 100, 30);
		contentPane.add(idReaderLabel);

		idReader_showLabel = new JLabel();
		idReader_showLabel.setBounds(220, 100, 300, 30);
		contentPane.add(idReader_showLabel);
		idReader_showLabel.setText(allReadersFrame.readerJtable.getValueAt(allReadersFrame.row, 0).toString());

		nameReaderLabel = new JLabel("Reader's Name");
		nameReaderLabel.setBounds(100, 200, 100, 30);
		contentPane.add(nameReaderLabel);

		nameReadertextField = new JTextField();
		nameReadertextField.setBounds(220, 200, 300, 30);
		contentPane.add(nameReadertextField);
		nameReadertextField.setColumns(10);
		nameReadertextField.setText(allReadersFrame.readerJtable.getValueAt(allReadersFrame.row, 1).toString());

		emailLabel = new JLabel("Reader's Email");
		emailLabel.setBounds(100, 300, 100, 30);
		contentPane.add(emailLabel);

		emailtextField = new JTextField();
		emailtextField.setBounds(220, 300, 300, 30);
		contentPane.add(emailtextField);
		emailtextField.setColumns(10);
		emailtextField.setText(allReadersFrame.readerJtable.getValueAt(allReadersFrame.row, 2).toString());

		passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(100, 400, 100, 30);
		contentPane.add(passwordLabel);

		passwordtextField = new JTextField();
		passwordtextField.setBounds(220, 400, 300, 30);
		contentPane.add(passwordtextField);
		passwordtextField.setColumns(10);
		passwordtextField.setText(allReadersFrame.readerJtable.getValueAt(allReadersFrame.row, 3).toString());
		
		updateButton = new JButton("update reader's information");
		updateButton.addActionListener(e -> do_updateButton_UpdateReader());
		updateButton.setBounds(200, 50, 200, 30);
		contentPane.add(updateButton);

	}

	protected void do_updateButton_UpdateReader() {

		ReaderTools readerTools = new ReaderTools();
		Reader reader = new Reader();
		
		if ( idReader_showLabel.getText() != null && !"".equals(idReader_showLabel.getText()) 
				&& nameReadertextField.getText() != null && !"".equals(nameReadertextField.getText())
				&& emailtextField.getText() != null && !"".equals(emailtextField.getText())
				&& passwordtextField.getText() != null && !"".equals(passwordtextField.getText()) ) {
			reader.setIdReader(Integer.parseInt(idReader_showLabel.getText()));
			reader.setNameReader(nameReadertextField.getText());
			reader.setEmailReader(emailtextField.getText());
			reader.setPasswordReader(passwordtextField.getText());

			int i = readerTools.UpdateReader(reader);
			if ( i == 1 ) {
				JOptionPane.showMessageDialog(getContentPane(), "update successfully!", "", JOptionPane.WARNING_MESSAGE);

			} else {
				JOptionPane.showMessageDialog(getContentPane(), "update failed,please try again", "", JOptionPane.WARNING_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(getContentPane(), "Please input all the reader's information", "", JOptionPane.WARNING_MESSAGE);
		}
		allReadersFrame allReadersFrame = new allReadersFrame();
		allReadersFrame.setVisible(true);
		CloseFrame();
	}
	public void CloseFrame(){
		super.dispose();
	}
}
