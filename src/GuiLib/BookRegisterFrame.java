package GuiLib;

import ModelLib.Author;
import ModelLib.Book;
import ModelLib.Publisher;
import DBLib.AuthorTools;
import DBLib.BookTools;
import DBLib.PublisherTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class BookRegisterFrame extends JFrame {

	private JPanel contentPane;

	private JTextField idBooktextField;
	private JTextField nameBooktextField;
	private JTextField kindtextField;
	private JTextField authorBirthdayField;
	private JTextField publisherPubdateField;
	private final JTextField nameAuthortextField;
	private final JTextField namePublishertextField;

	private final JLabel namePublisherLabel;
	private final JLabel nameAuthorLabel;
	private JLabel idBookLabel;
	private JLabel nameBookLabel;
	private JLabel kindLabel;
	private JLabel authorBirthdayLabel;
	private JLabel publisherPubdateLabel;

	private JButton insertButton;

	public BookRegisterFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton reader_Registerbutton = new JButton("Reader registration");
		reader_Registerbutton.addActionListener(e -> {
			ReaderRegisterFrame reader_RegisterFrame = new ReaderRegisterFrame();
			reader_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton book_Registerbutton = new JButton("Book registration");
		book_Registerbutton.addActionListener(e -> {
			BookRegisterFrame book_RegisterFrame = new BookRegisterFrame();
			book_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Readerbutton = new JButton("All readers");
		all_Readerbutton.addActionListener(e -> {
			allReadersFrame allReadersFrame = new allReadersFrame();
			allReadersFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Bookbutton = new JButton("All books");
		all_Bookbutton.addActionListener(e -> {
			allBooksFrame allBooksFrame = new allBooksFrame();
			allBooksFrame.setVisible(true);
			CloseFrame();
		});

		JButton checkReader_button = new JButton("Borrowing history");
		checkReader_button.addActionListener(e -> {
			BorrowingHistoryFrame borrowingHistoryFrame = new BorrowingHistoryFrame();
			borrowingHistoryFrame.setVisible(true);
			CloseFrame();
		});

		JButton log_out_Button = new JButton("Log out");
		log_out_Button.addActionListener(e -> {
			LogInFrame loginframe = new LogInFrame();
			loginframe.setVisible(true);
			CloseFrame();
		});

		JButton blacklistbutton = new JButton("Blacklist");
		blacklistbutton.addActionListener(e -> {
			blacklistFrame blacklistFrame = new blacklistFrame();
			blacklistFrame.setVisible(true);
			CloseFrame();
		});
		reader_Registerbutton.setBounds(20, 40, 170, 30);
		contentPane.add(reader_Registerbutton);
		book_Registerbutton.setBounds(20, 120, 170, 30);
		contentPane.add(book_Registerbutton);
		all_Readerbutton.setBounds(20, 200, 170, 30);
		contentPane.add(all_Readerbutton);
		all_Bookbutton.setBounds(20, 280, 170, 30);
		contentPane.add(all_Bookbutton);
		checkReader_button.setBounds(20, 360, 170, 30);
		contentPane.add(checkReader_button);
		blacklistbutton.setBounds(20, 440, 170, 30);
		contentPane.add(blacklistbutton);
		log_out_Button.setBounds(340, 500, 170, 40);
		contentPane.add(log_out_Button);


		idBookLabel = new JLabel("ID number");
		idBookLabel.setBounds(280, 120, 100, 30);
		contentPane.add(idBookLabel);

		idBooktextField = new JTextField();
		idBooktextField.setBounds(400, 120, 300, 30);
		contentPane.add(idBooktextField);
		idBooktextField.setColumns(10);

		nameBookLabel = new JLabel("name of book");
		nameBookLabel.setBounds(280, 170, 100, 30);
		contentPane.add(nameBookLabel);

		nameBooktextField = new JTextField();
		nameBooktextField.setBounds(400, 170, 300, 30);
		contentPane.add(nameBooktextField);
		nameBooktextField.setColumns(10);

		kindLabel = new JLabel("kind of book");
		kindLabel.setBounds(280, 220, 100, 30);
		contentPane.add(kindLabel);

		kindtextField = new JTextField();
		kindtextField.setBounds(400, 220, 300, 30);
		contentPane.add(kindtextField);
		kindtextField.setColumns(10);

		nameAuthorLabel = new JLabel("author");
		nameAuthorLabel.setBounds(280, 270, 100, 30);
		contentPane.add(nameAuthorLabel);

		nameAuthortextField = new JTextField();
		nameAuthortextField.setBounds(400, 270, 300, 30);
		contentPane.add(nameAuthortextField);
		nameAuthortextField.setColumns(10);

		authorBirthdayLabel = new JLabel("author's birthyear");
		authorBirthdayLabel.setBounds(280, 320, 120, 30);
		contentPane.add(authorBirthdayLabel);

		authorBirthdayField = new JTextField();
		authorBirthdayField.setBounds(400, 320, 300, 30);
		contentPane.add(authorBirthdayField);
		authorBirthdayField.setColumns(10);

		namePublisherLabel = new JLabel("publisher");
		namePublisherLabel.setBounds(280, 370, 100, 30);
		contentPane.add(namePublisherLabel);

		namePublishertextField = new JTextField();
		namePublishertextField.setBounds(400, 370, 300, 30);
		contentPane.add(namePublishertextField);
		namePublishertextField.setColumns(10);

		publisherPubdateLabel = new JLabel("publication year");
		publisherPubdateLabel.setBounds(280, 420, 100, 30);
		contentPane.add(publisherPubdateLabel);

		publisherPubdateField = new JTextField();
		publisherPubdateField.setBounds(400, 420, 300, 30);
		contentPane.add(publisherPubdateField);
		publisherPubdateField.setColumns(10);

		insertButton = new JButton("add a new book");
		insertButton.addActionListener(e -> do_insertButton_AddBook());
		insertButton.setBounds(400, 50, 150, 40);
		contentPane.add(insertButton);

	}

	protected void do_insertButton_AddBook() {

		BookTools bookTools = new BookTools();
		Book book = new Book();

		Author author = new Author();
		AuthorTools authorTools = new AuthorTools();

		Publisher publisher = new Publisher();
		PublisherTools publisherTools = new PublisherTools();

		if (idBooktextField.getText() != null && !"".equals(idBooktextField.getText())
				&& nameBooktextField.getText() != null && !"".equals(nameBooktextField.getText())
				&& !"".equals(kindtextField.getText()) && kindtextField.getText() != null
				&& !"".equals(nameAuthortextField.getText()) && nameAuthortextField.getText() != null
				&& !"".equals(authorBirthdayField.getText()) && authorBirthdayField.getText() != null
				&& !"".equals(namePublishertextField.getText()) && namePublishertextField.getText() != null
				&& !"".equals(publisherPubdateField.getText()) && publisherPubdateField.getText() != null) {

			book.setIdBook(Integer.parseInt(idBooktextField.getText()));
			book.setNameBook(nameBooktextField.getText());
			book.setKind(kindtextField.getText());
			book.setNameAuthor(nameAuthortextField.getText());
			book.setNamePublisher(namePublishertextField.getText());
			author.setNameAuthor(nameAuthortextField.getText());
			author.setBirthyear(Integer.parseInt(authorBirthdayField.getText()));
			publisher.setNamePublisher(namePublishertextField.getText());
			publisher.setPubyear(Integer.parseInt(publisherPubdateField.getText()));

			publisherTools.AddPublisher(publisher);
			authorTools.AddAuthor(author);

			int i = bookTools.AddBook(book);
			if (i == 1) {
				JOptionPane.showMessageDialog(getContentPane(), "Register successfully", "", JOptionPane.WARNING_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(getContentPane(), "Register failed,please try again", "", JOptionPane.WARNING_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(getContentPane(), "Please input all the book's information", "", JOptionPane.WARNING_MESSAGE);
		}
	}

	public void CloseFrame() {
		super.dispose();
	}
}
