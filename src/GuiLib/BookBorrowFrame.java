package GuiLib;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ModelLib.Book;
import DBLib.BookTools;
import DBLib.BorrowTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class BookBorrowFrame extends JFrame {

	private JPanel contentPane;

	private JScrollPane bookScrollPane;
	public JTable bookJtable;
	private DefaultTableModel defaultModel;
	public static int row;

	private JLabel messageLabel;
	private JTextField nameBookField;
	private JButton searchButton;

	private JButton borrowButton;


	public BookBorrowFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton borrow_Button = new JButton("Borrow a book");
		borrow_Button.addActionListener(e -> {
			BookBorrowFrame _BookBorrowFrame = new BookBorrowFrame();
			_BookBorrowFrame.setVisible(true);
			CloseFrame();
		});

		JButton self_info_Button = new JButton("My borrowing information");
		self_info_Button.addActionListener(e -> {
			ReaderInfoFrame _ReaderInfoFrame = new ReaderInfoFrame();
			_ReaderInfoFrame.setVisible(true);
			CloseFrame();
		});

		JButton log_out_Button = new JButton("Log out");
		log_out_Button.addActionListener(e -> {
			LogInFrame loginframe = new LogInFrame();
			loginframe.setVisible(true);
			CloseFrame();
		});

		borrow_Button.setBounds(140, 20, 200, 30);
		contentPane.add(borrow_Button);
		self_info_Button.setBounds(540, 20, 200, 30);
		contentPane.add(self_info_Button);
		log_out_Button.setBounds(340, 500, 120, 40);
		contentPane.add(log_out_Button);

		messageLabel = new JLabel("Search by book's name");
		messageLabel.setBounds(340, 70, 200, 40);
		contentPane.add(messageLabel);

		nameBookField = new JTextField();
		nameBookField.setBounds(250, 110, 300, 30);
		contentPane.add(nameBookField);
		nameBookField.setColumns(10);

		searchButton = new JButton("keyword search");
		searchButton.addActionListener(e -> do_search_book());
		searchButton.setBounds(560, 110, 180, 30);
		contentPane.add(searchButton);

		bookScrollPane = new JScrollPane(bookJtable);
		bookScrollPane.setBounds(50, 150, 700, 330);
		contentPane.add(bookScrollPane);

		borrowButton = new JButton("borrow this book");
		borrowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_borrow_book();
			}
		});
		borrowButton.setBounds(55, 110, 150, 35);
		contentPane.add(borrowButton);

		show_data();
	}

	private void show_data() {

		bookJtable = new JTable();
		bookJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		bookJtable.setRowHeight(54);

		defaultModel = (DefaultTableModel) bookJtable.getModel();
		defaultModel.setRowCount(0);
		defaultModel.setColumnIdentifiers(new Object[] { "Book's ID", "Book's Name", "Book's Kind", "Author", "Publisher", "Whether in Stock" });

		bookJtable.getTableHeader().setReorderingAllowed(false);
		bookJtable.setModel(defaultModel);

		bookJtable.getColumnModel().getColumn(0).setPreferredWidth(10);
		bookJtable.getColumnModel().getColumn(1).setPreferredWidth(80);
		bookJtable.getColumnModel().getColumn(2).setPreferredWidth(20);
		bookJtable.getColumnModel().getColumn(3).setPreferredWidth(20);
		bookJtable.getColumnModel().getColumn(4).setPreferredWidth(10);
		bookJtable.getColumnModel().getColumn(5).setPreferredWidth(90);

		BookTools bookTools = new BookTools();

		List<Book> booklist = bookTools.BookData();
		BorrowTools borrowtools = new BorrowTools();

		for (Book temp : booklist) {
			String whetherInStock;

			if (borrowtools.whetherInStock(temp.getIdBook())) {
				whetherInStock = "in Stock";
			} else {
				whetherInStock = "Not in Stock";
			}
			defaultModel.addRow(new Object[]{temp.getIdBook(), temp.getNameBook(), temp.getKind(),
					temp.getNameAuthor(), temp.getNamePublisher(), whetherInStock});
		}
		bookScrollPane.setViewportView(bookJtable);
	}

	private void do_search_book() {

		bookJtable = new JTable();
		bookJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		bookJtable.setRowHeight(54);

		defaultModel = (DefaultTableModel) bookJtable.getModel();
		defaultModel.setRowCount(0);
		defaultModel.setColumnIdentifiers(new Object[] { "Book's ID", "Book's Name", "Book's Kind", "Author", "Publisher", "Whether in Stock" });

		bookJtable.getTableHeader().setReorderingAllowed(false);
		bookJtable.setModel(defaultModel);

		bookJtable.getColumnModel().getColumn(0).setPreferredWidth(10);
		bookJtable.getColumnModel().getColumn(1).setPreferredWidth(80);
		bookJtable.getColumnModel().getColumn(2).setPreferredWidth(20);
		bookJtable.getColumnModel().getColumn(3).setPreferredWidth(20);
		bookJtable.getColumnModel().getColumn(4).setPreferredWidth(10);
		bookJtable.getColumnModel().getColumn(5).setPreferredWidth(90);

		BookTools booktools = new BookTools();
		BorrowTools borrowtools = new BorrowTools();

		String keyword;
		if (nameBookField.getText() != null && !"".equals(nameBookField.getText())) {
			keyword = nameBookField.getText();
		} else {
			JOptionPane.showMessageDialog(this, "Please input book's name", "", JOptionPane.WARNING_MESSAGE);
			return;
		}

		List<Book> booklist = booktools.BookData(keyword);

		if (booklist.size() == 0) {
			JOptionPane.showMessageDialog(this, "Book not found, please try again", "", JOptionPane.WARNING_MESSAGE);
		} else {
			for (Book temp : booklist) {
				String whetherInStock;
				if (borrowtools.whetherInStock(temp.getIdBook())) {
					whetherInStock = "in Stock";
				} else {
					whetherInStock = "Not in Stock";
				}
				defaultModel.addRow(new Object[]{temp.getIdBook(), temp.getNameBook(),
						temp.getKind(), temp.getNameAuthor(), temp.getNamePublisher(), whetherInStock});
			}
			bookScrollPane.setViewportView(bookJtable);
		}
	}

	private void do_borrow_book() {

		row = bookJtable.getSelectedRow();
		if (row == -1) {
			JOptionPane.showMessageDialog(this, "Select a book", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		if ("Not in Stock".equals(bookJtable.getValueAt(row, 5).toString())) {
			JOptionPane.showMessageDialog(this, "Book has already been borrowed", "", JOptionPane.WARNING_MESSAGE);
		} else {
			BorrowTools borrowtools = new BorrowTools();
			int idReader = Integer.parseInt(String.valueOf(LogInFrame.idReader));
			int idBook = Integer.parseInt(bookJtable.getValueAt(row, 0).toString());
			String nameBook = bookJtable.getValueAt(row, 1).toString();
			int i = borrowtools.BorrowBook(idReader, idBook);
			int j = borrowtools.BorrowHistory(idReader,idBook,nameBook);
			System.out.println("borrow"+i+"+"+j);
			if (i == 1 && j != 0) {
				JOptionPane.showMessageDialog(this, "Borrow successfully", "", JOptionPane.WARNING_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this, "Borrow failed, please try again", "", JOptionPane.WARNING_MESSAGE);
			}
			show_data();
		}
	}

	public void CloseFrame() {
		super.dispose();
	}
}
