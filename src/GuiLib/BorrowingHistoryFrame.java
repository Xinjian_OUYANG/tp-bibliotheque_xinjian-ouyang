package GuiLib;

import ModelLib.Reader;
import DBLib.BorrowTools;
import DBLib.ReaderTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class BorrowingHistoryFrame extends JFrame {
 	private JPanel contentPane;

	private JScrollPane bookScrollPane;
	private JTable bookJtable;
	private DefaultTableModel defaultModel;

	private JLabel idReaderLabel;
	private JLabel nameReaderLabel;
	private JLabel emailLabel;
	private JLabel passwordLabel;
	private JTextField idReadertextField;

	private JLabel showNameReaderLabel;
	private JLabel showemailLabel;
	private JLabel showPasswordLabel;

	public BorrowingHistoryFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton reader_Registerbutton = new JButton("Reader registration");
		reader_Registerbutton.addActionListener(e -> {
			ReaderRegisterFrame reader_RegisterFrame = new ReaderRegisterFrame();
			reader_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton book_Registerbutton = new JButton("Book registration");
		book_Registerbutton.addActionListener(e -> {
			BookRegisterFrame book_RegisterFrame = new BookRegisterFrame();
			book_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Readerbutton = new JButton("All readers");
		all_Readerbutton.addActionListener(e -> {
			allReadersFrame allReadersFrame = new allReadersFrame();
			allReadersFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Bookbutton = new JButton("All books");
		all_Bookbutton.addActionListener(e -> {
			allBooksFrame allBooksFrame = new allBooksFrame();
			allBooksFrame.setVisible(true);
			CloseFrame();
		});

		JButton checkReader_button = new JButton("Borrowing history");
		checkReader_button.addActionListener(e -> {
			BorrowingHistoryFrame borrowingHistoryFrame = new BorrowingHistoryFrame();
			borrowingHistoryFrame.setVisible(true);
			CloseFrame();
		});

		JButton log_out_Button = new JButton("Log out");
		log_out_Button.addActionListener(e -> {
			LogInFrame loginframe = new LogInFrame();
			loginframe.setVisible(true);
			CloseFrame();
		});

		JButton blacklistbutton = new JButton("Blacklist");
		blacklistbutton.addActionListener(e -> {
			blacklistFrame blacklistFrame = new blacklistFrame();
			blacklistFrame.setVisible(true);
			CloseFrame();
		});

		reader_Registerbutton.setBounds(20, 40, 170, 30);
		contentPane.add(reader_Registerbutton);
		book_Registerbutton.setBounds(20, 120, 170, 30);
		contentPane.add(book_Registerbutton);
		all_Readerbutton.setBounds(20, 200, 170, 30);
		contentPane.add(all_Readerbutton);
		all_Bookbutton.setBounds(20, 280, 170, 30);
		contentPane.add(all_Bookbutton);
		checkReader_button.setBounds(20, 360, 170, 30);
		contentPane.add(checkReader_button);
		blacklistbutton.setBounds(20, 440, 170, 30);
		contentPane.add(blacklistbutton);
		log_out_Button.setBounds(340, 500, 170, 40);
		contentPane.add(log_out_Button);

		idReaderLabel = new JLabel("Reader's ID");
		idReaderLabel.setBounds(250, 20, 100, 30);
		contentPane.add(idReaderLabel);

		idReadertextField = new JTextField();
		idReadertextField.setBounds(350, 20, 300, 30);
		idReadertextField.setColumns(10);
		contentPane.add(idReadertextField);

		JButton btnNewButton = new JButton("check");
		btnNewButton.addActionListener(e -> show_data());
		btnNewButton.setBounds(655, 20, 100, 30);
		contentPane.add(btnNewButton);

		nameReaderLabel = new JLabel("Reader's Name");
		nameReaderLabel.setBounds(250, 50, 100, 30);
		contentPane.add(nameReaderLabel);

		showNameReaderLabel = new JLabel("");
		showNameReaderLabel.setBounds(350, 50, 300, 30);
		contentPane.add(showNameReaderLabel);

		emailLabel = new JLabel("Reader's email");
		emailLabel.setBounds(250, 80, 100, 30);
		contentPane.add(emailLabel);

		showemailLabel = new JLabel("");
		showemailLabel.setBounds(350, 80, 300, 30);
		contentPane.add(showemailLabel);

		passwordLabel = new JLabel("password");
		passwordLabel.setBounds(250, 110, 100, 30);
		contentPane.add(passwordLabel);

		showPasswordLabel = new JLabel("");
		showPasswordLabel.setBounds(350, 110, 300, 30);
		contentPane.add(showPasswordLabel);


		bookScrollPane = new JScrollPane(bookJtable);
		bookScrollPane.setBounds(250, 145, 500, 320);
		contentPane.add(bookScrollPane);

	}

	private void show_data() {

		bookJtable = new JTable();
		bookJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		bookJtable.setRowHeight(54);

		defaultModel = (DefaultTableModel) bookJtable.getModel();
		defaultModel.setRowCount(0);
		defaultModel.setColumnIdentifiers(new Object[] { "Reader's ID","Book's ID", "Book's Name", "Borrowing Date","Return Date","Is Returned" });

		bookJtable.getTableHeader().setReorderingAllowed(false);
		bookJtable.setModel(defaultModel);

		bookJtable.getColumnModel().getColumn(0).setPreferredWidth(3);
		bookJtable.getColumnModel().getColumn(1).setPreferredWidth(3);
		bookJtable.getColumnModel().getColumn(2).setPreferredWidth(40);
		bookJtable.getColumnModel().getColumn(3).setPreferredWidth(50);
		bookJtable.getColumnModel().getColumn(4).setPreferredWidth(50);
		bookJtable.getColumnModel().getColumn(5).setPreferredWidth(3);


		ReaderTools readerTools = new ReaderTools();
		Reader reader = new Reader();
		BorrowTools borrowtools = new BorrowTools();

		if (idReadertextField.getText() != null && !"".equals(idReadertextField.getText())) {
			reader.setIdReader(Integer.parseInt(idReadertextField.getText()));
		} else {
			JOptionPane.showMessageDialog(this, "Please input reader's ID", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		List<Reader> readerlist = readerTools.ReaderData(reader.getIdReader());
		List<ModelLib.BorrowingHistory> Historylist = borrowtools.HistoryData(reader.getIdReader());

		// Check the idReader
		if (readerlist.size() == 0) {
			JOptionPane.showMessageDialog(this, "ID error, please input the correct reader's ID", "", JOptionPane.WARNING_MESSAGE);
			return;
		} else {
			for (Reader temp : readerlist) {
				showNameReaderLabel.setText(temp.getNameReader());
				showemailLabel.setText(temp.getEmailReader());
				showPasswordLabel.setText(temp.getPasswordReader());
			}
			for (ModelLib.BorrowingHistory temp : Historylist) {
				defaultModel.addRow(new Object[]{temp.getIdReader(), temp.getIdBook(),
						temp.getNameBook(), temp.getBorrowDate(),temp.getReturnDate(), temp.getIsReturned()});
			}
		}
		bookScrollPane.setViewportView(bookJtable);
	}

	public void CloseFrame() {
		super.dispose();
	}
}
