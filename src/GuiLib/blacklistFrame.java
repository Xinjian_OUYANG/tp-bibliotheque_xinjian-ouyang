package GuiLib;
import DBLib.BlacklistTools;
import ModelLib.Blacklist;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class blacklistFrame extends JFrame {
    private JPanel contentPane;

    private JScrollPane blacklistScrollPane;
    public JTable blacklistJtable;
    private DefaultTableModel defaultModel;

    private JButton deleteButton;
    private JButton searchButton;
    private JTextField searchtextField;

    public static void main(String[] args) {
        try {
            blacklistFrame frame = new blacklistFrame();
            frame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CloseFrame() {
        super.dispose();
    }

    public blacklistFrame() {
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 800, 600);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JButton reader_Registerbutton = new JButton("Reader registration");
        reader_Registerbutton.addActionListener(e -> {
            ReaderRegisterFrame reader_RegisterFrame = new ReaderRegisterFrame();
            reader_RegisterFrame.setVisible(true);
            CloseFrame();
        });

        JButton book_Registerbutton = new JButton("Book registration");
        book_Registerbutton.addActionListener(e -> {
            BookRegisterFrame book_RegisterFrame = new BookRegisterFrame();
            book_RegisterFrame.setVisible(true);
            CloseFrame();
        });

        JButton all_Readerbutton = new JButton("All readers");
        all_Readerbutton.addActionListener(e -> {
            allReadersFrame allReadersFrame = new allReadersFrame();
            allReadersFrame.setVisible(true);
            CloseFrame();
        });

        JButton all_Bookbutton = new JButton("All books");
        all_Bookbutton.addActionListener(e -> {
            allBooksFrame allBooksFrame = new allBooksFrame();
            allBooksFrame.setVisible(true);
            CloseFrame();
        });

        JButton checkReader_button = new JButton("Borrowing history");
        checkReader_button.addActionListener(e -> {
            BorrowingHistoryFrame borrowingHistoryFrame = new BorrowingHistoryFrame();
            borrowingHistoryFrame.setVisible(true);
            CloseFrame();
        });

        JButton blacklistbutton = new JButton("Blacklist");
        blacklistbutton.addActionListener(e -> {
            blacklistFrame blacklistFrame = new blacklistFrame();
            blacklistFrame.setVisible(true);
            CloseFrame();
        });

        JButton log_out_Button = new JButton("Log out");
        log_out_Button.addActionListener(e -> {
            LogInFrame loginframe = new LogInFrame();
            loginframe.setVisible(true);
            CloseFrame();
        });

        reader_Registerbutton.setBounds(20, 40, 170, 30);
        contentPane.add(reader_Registerbutton);
        book_Registerbutton.setBounds(20, 120, 170, 30);
        contentPane.add(book_Registerbutton);
        all_Readerbutton.setBounds(20, 200, 170, 30);
        contentPane.add(all_Readerbutton);
        all_Bookbutton.setBounds(20, 280, 170, 30);
        contentPane.add(all_Bookbutton);
        checkReader_button.setBounds(20, 360, 170, 30);
        contentPane.add(checkReader_button);
        blacklistbutton.setBounds(20, 440, 170, 30);
        contentPane.add(blacklistbutton);
        log_out_Button.setBounds(340, 500, 170, 40);
        contentPane.add(log_out_Button);

        blacklistScrollPane = new JScrollPane();
        blacklistScrollPane.setBounds(250, 70, 500, 340);
        contentPane.add(blacklistScrollPane);

        searchtextField = new JTextField();
        searchtextField.setBounds(270, 30, 300, 30);
        contentPane.add(searchtextField);
        searchtextField.setColumns(10);

        searchButton = new JButton("keyword search by name");
        searchButton.addActionListener(e -> do_search_blackuser());
        searchButton.setBounds(580, 30, 180, 30);
        contentPane.add(searchButton);

        deleteButton = new JButton("delete");
        deleteButton.addActionListener(e -> delete_data());
        deleteButton.setBounds(550, 430, 100, 30);
        contentPane.add(deleteButton);

        show_data();
    }

    private void show_data() {

        blacklistJtable = new JTable();
        blacklistJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        blacklistJtable.setRowHeight(54);

        defaultModel = (DefaultTableModel) blacklistJtable.getModel();
        defaultModel.setRowCount(0);
        defaultModel.setColumnIdentifiers(new Object[] { "ID", "Name" });

        blacklistJtable.getTableHeader().setReorderingAllowed(false);
        blacklistJtable.setModel(defaultModel);

        blacklistJtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        blacklistJtable.getColumnModel().getColumn(1).setPreferredWidth(40);

        BlacklistTools blacklistTools = new BlacklistTools();
        List<Blacklist> blacklist = blacklistTools.BlacklistData();

        for (Blacklist temp : blacklist) {
            defaultModel.addRow(new Object[]{temp.getIdReaderBlack(), temp.getNameReaderBlack()});
        }
        blacklistScrollPane.setViewportView(blacklistJtable);
    }

    private void do_search_blackuser() {

        blacklistJtable = new JTable();
        blacklistJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        blacklistJtable.setRowHeight(54);

        defaultModel = (DefaultTableModel) blacklistJtable.getModel();
        defaultModel.setRowCount(0);
        defaultModel.setColumnIdentifiers(new Object[] { "ID", "Name" });

        blacklistJtable.getTableHeader().setReorderingAllowed(false);
        blacklistJtable.setModel(defaultModel);

        blacklistJtable.getColumnModel().getColumn(0).setPreferredWidth(10);
        blacklistJtable.getColumnModel().getColumn(1).setPreferredWidth(40);

        BlacklistTools blacklistTools = new BlacklistTools();

        String keyword;
        if (searchtextField.getText() != null && !"".equals(searchtextField.getText())) {
            keyword = searchtextField.getText();
        } else {
            show_data();
            JOptionPane.showMessageDialog(this, "Please input black user's name", "", JOptionPane.WARNING_MESSAGE);
            return;
        }

        List<Blacklist> blacklist = blacklistTools.BlackuserSearch(keyword);

        if (blacklist.size() == 0) {
            JOptionPane.showMessageDialog(this, "User not found ", "", JOptionPane.WARNING_MESSAGE);
        } else {
            for (Blacklist temp : blacklist) {
                defaultModel.addRow(new Object[]{temp.getIdReaderBlack(), temp.getNameReaderBlack()});
                JOptionPane.showMessageDialog(this, "User found", "", JOptionPane.WARNING_MESSAGE);
            }
            blacklistScrollPane.setViewportView(blacklistJtable);
        }

    }

    private void delete_data() {
        int row = blacklistJtable.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Select a black user", "", JOptionPane.WARNING_MESSAGE);
            return;
        }
        BlacklistTools blacklistTools = new BlacklistTools();
        int i = blacklistTools.DeleteBlackuser(Integer.parseInt(blacklistJtable.getValueAt(row, 0).toString()));
        if (i == 1) {
            JOptionPane.showMessageDialog(getContentPane(), "Delete Successfully", "", JOptionPane.WARNING_MESSAGE);
            this.show_data();
        } else {
            JOptionPane.showMessageDialog(getContentPane(), "Delete failed", "", JOptionPane.WARNING_MESSAGE);
        }
    }

}
