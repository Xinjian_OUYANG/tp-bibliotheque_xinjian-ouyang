package GuiLib;

import ModelLib.Librarian;
import ModelLib.Reader;
import DBLib.LibrarianTools;
import DBLib.ReaderTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LogInFrame extends JFrame implements ItemListener {

    private JPanel contentPane;

    private boolean readerLogin = true;
    private boolean librarianLogin = false;
    public static int idReader;
    public static String nameReader;
    public static String nameLibrarian;
    private Librarian lib;
    private Reader reader;

    private JTextField nameLibrarianTextField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JButton view_Password_Button;

    private JRadioButton readerRadioButton;
    private JRadioButton librarianRadioButton;
    private ButtonGroup group;

    private JLabel userNameLabel;
    private JLabel passwordLabel;
    private JLabel welcomeLabel;

    //create the login frame
    public LogInFrame() {
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 400, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        welcomeLabel = new JLabel("Welcome to our library");
        welcomeLabel.setForeground(Color.BLACK);
        welcomeLabel.setBounds(130, 20, 300, 30);
        contentPane.add(welcomeLabel);

        userNameLabel = new JLabel("UserID");
        userNameLabel.setForeground(Color.BLACK);
        userNameLabel.setBounds(30, 80, 100, 20);
        contentPane.add(userNameLabel);

        nameLibrarianTextField = new JTextField();
        nameLibrarianTextField.setBounds(130, 80, 170, 20);
        contentPane.add(nameLibrarianTextField);
        nameLibrarianTextField.setColumns(10);

        passwordLabel = new JLabel("Password");
        passwordLabel.setForeground(Color.BLACK);
        passwordLabel.setBounds(30, 120, 100, 20);
        contentPane.add(passwordLabel);

        passwordField = new JPasswordField();
        passwordField.setBounds(130, 120, 170, 20);
        contentPane.add(passwordField);

        view_Password_Button = new JButton("view");
        view_Password_Button.setBounds(300, 120, 70, 20);
        view_Password_Button.addMouseListener(new MouseAdapter(){
            char echoChar = passwordField.getEchoChar();
            public void mousePressed(MouseEvent e){
                passwordField.setEchoChar((char)0);
            }
            public void mouseReleased(MouseEvent e){
                passwordField.setEchoChar(echoChar);
                passwordField.requestFocus();
            }
        });
        contentPane.add(view_Password_Button);

        // events listener, JRadioButton. Default: readerRadioButton
        readerRadioButton = new JRadioButton("Reader");
        readerRadioButton.setBounds(100, 150, 120, 25);
        contentPane.add(readerRadioButton);
        readerRadioButton.addItemListener(this);
        readerRadioButton.setSelected(true);
        readerRadioButton.setContentAreaFilled(false);

        librarianRadioButton = new JRadioButton("Librarian");
        librarianRadioButton.setBounds(200, 150, 120, 25);
        contentPane.add(librarianRadioButton);
        librarianRadioButton.addItemListener(this);
        librarianRadioButton.setContentAreaFilled(false);

        // put the single-option buttons in a group
        group = new ButtonGroup();
        group.add(this.readerRadioButton);
        group.add(this.librarianRadioButton);

        loginButton = new JButton("Log in");
        loginButton.setFont(new Font("Dialog", Font.BOLD, 15));
        loginButton.addActionListener(e -> check_login());
        loginButton.setBounds(130, 200, 120, 40);
        contentPane.add(loginButton);

    }

    private void check_login() {
        if (this.readerLogin) {
            ReaderTools rTools = new ReaderTools();
            reader = new Reader();
            reader.setIdReader(Integer.parseInt(nameLibrarianTextField.getText()));
            reader.setPasswordReader(new String(passwordField.getPassword()));

            if (nameLibrarianTextField.getText() != null && !"".equals(nameLibrarianTextField.getText())
                    && passwordField.getPassword() != null && !("".equals(new String(passwordField.getPassword())))) {
                boolean whether_login = rTools.ReaderLogin(reader.getIdReader(), reader.getPasswordReader());
                if(whether_login) {
                    idReader = reader.getIdReader();
                    //check if the user is in the blacklist
                    if (rTools.isInBlacklist(idReader)){
                        JOptionPane.showMessageDialog(this, "Sorry you are in the blacklist!", "", JOptionPane.WARNING_MESSAGE);
                    }else{
                        ReaderInfoFrame ReaderInfoFrame = new ReaderInfoFrame();
                        ReaderInfoFrame.setVisible(true);
                        CloseFrame();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Wrong Username or Password! Try again", "", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please input your username and password", "", JOptionPane.WARNING_MESSAGE);
            }
            return;
        }

        if (this.librarianLogin) {
            LibrarianTools libTools = new LibrarianTools();
            lib = new Librarian();
            lib.setNameLibrarian(nameLibrarianTextField.getText());
            lib.setPasswordLibrarian(new String(passwordField.getPassword()));
            if (nameLibrarianTextField.getText() != null && !"".equals(nameLibrarianTextField.getText())
                    && passwordField.getPassword() != null && !("".equals(new String(passwordField.getPassword())))) {
                boolean whether_login = libTools.LibrarianLogin(lib.getNameLibrarian(), lib.getPasswordLibrarian());
                if (whether_login) {
                    nameLibrarian = lib.getNameLibrarian();
                    allBooksFrame allBooksFrame = new allBooksFrame();
                    allBooksFrame.setVisible(true);
                    CloseFrame();
                } else {
                    JOptionPane.showMessageDialog(this, "Wrong Username or Password! Try again", "", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please input your username and password", "", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == readerRadioButton) {
            this.readerLogin = true;
            this.librarianLogin = false;
        } else {
            this.readerLogin = false;
            this.librarianLogin = true;
        }
    }
    public void CloseFrame() {
        super.dispose();
    }
}
