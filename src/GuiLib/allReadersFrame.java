package GuiLib;

import DBLib.BlacklistTools;
import ModelLib.Blacklist;
import ModelLib.Reader;
import DBLib.ReaderTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class allReadersFrame extends JFrame {

	private JPanel contentPane;

	private JScrollPane readerScrollPane;
	public JTable readerJtable;
	private DefaultTableModel defaultModel;
	public int row;

	private JButton blacklistButton;
	private JButton updateButton;
	private JButton deleteButton;
	private JButton searchButton;
	private JTextField searchtextField;

	public static void main(String[] args) {
		try {
			allReadersFrame frame = new allReadersFrame();
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Create a frame.
	public allReadersFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton reader_Registerbutton = new JButton("Reader registration");
		reader_Registerbutton.addActionListener(e -> {
			ReaderRegisterFrame reader_RegisterFrame = new ReaderRegisterFrame();
			reader_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton book_Registerbutton = new JButton("Book registration");
		book_Registerbutton.addActionListener(e -> {
			BookRegisterFrame book_RegisterFrame = new BookRegisterFrame();
			book_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Readerbutton = new JButton("All readers");
		all_Readerbutton.addActionListener(e -> {
			allReadersFrame allReadersFrame = new allReadersFrame();
			allReadersFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Bookbutton = new JButton("All books");
		all_Bookbutton.addActionListener(e -> {
			allBooksFrame allBooksFrame = new allBooksFrame();
			allBooksFrame.setVisible(true);
			CloseFrame();
		});

		JButton checkReader_button = new JButton("Borrowing history");
		checkReader_button.addActionListener(e -> {
			BorrowingHistoryFrame borrowingHistoryFrame = new BorrowingHistoryFrame();
			borrowingHistoryFrame.setVisible(true);
			CloseFrame();
		});

		JButton log_out_Button = new JButton("Log out");
		log_out_Button.addActionListener(e -> {
			LogInFrame loginframe = new LogInFrame();
			loginframe.setVisible(true);
			CloseFrame();
		});

		JButton blacklistbutton = new JButton("Blacklist");
		blacklistbutton.addActionListener(e -> {
			blacklistFrame blacklistFrame = new blacklistFrame();
			blacklistFrame.setVisible(true);
			CloseFrame();
		});
		reader_Registerbutton.setBounds(20, 40, 170, 30);
		contentPane.add(reader_Registerbutton);
		book_Registerbutton.setBounds(20, 120, 170, 30);
		contentPane.add(book_Registerbutton);
		all_Readerbutton.setBounds(20, 200, 170, 30);
		contentPane.add(all_Readerbutton);
		all_Bookbutton.setBounds(20, 280, 170, 30);
		contentPane.add(all_Bookbutton);
		checkReader_button.setBounds(20, 360, 170, 30);
		contentPane.add(checkReader_button);
		blacklistbutton.setBounds(20, 440, 170, 30);
		contentPane.add(blacklistbutton);
		log_out_Button.setBounds(340, 500, 170, 40);
		contentPane.add(log_out_Button);

		readerScrollPane = new JScrollPane();
		readerScrollPane.setBounds(250, 70, 500, 340);
		contentPane.add(readerScrollPane);

		searchtextField = new JTextField();
		searchtextField.setBounds(270, 30, 300, 30);
		contentPane.add(searchtextField);
		searchtextField.setColumns(10);

		searchButton = new JButton("keyword search by name");
		searchButton.addActionListener(e -> do_search_reader());
		searchButton.setBounds(580, 30, 180, 30);
		contentPane.add(searchButton);

		updateButton = new JButton("update");
		updateButton.addActionListener(e -> update_Reader());
		updateButton.setBounds(450, 430, 100, 30);
		contentPane.add(updateButton);

		deleteButton = new JButton("delete");
		deleteButton.addActionListener(e -> delete_data());
		deleteButton.setBounds(600, 430, 100, 30);
		contentPane.add(deleteButton);

		blacklistButton = new JButton("add to blacklist");
		blacklistButton.addActionListener(e -> addBlackuser());
		blacklistButton.setBounds(280, 430, 120, 30);
		contentPane.add(blacklistButton);

		show_data();

	}

	// show all readers' information
	private void show_data() {

		readerJtable = new JTable();
		readerJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		readerJtable.setRowHeight(54);

		defaultModel = (DefaultTableModel) readerJtable.getModel();
		defaultModel.setRowCount(0);
		defaultModel.setColumnIdentifiers(new Object[] { "Reader's ID", "Reader's Name", "Reader's Email", "Password" });

		readerJtable.getTableHeader().setReorderingAllowed(false);
		readerJtable.setModel(defaultModel);

		readerJtable.getColumnModel().getColumn(0).setPreferredWidth(10);
		readerJtable.getColumnModel().getColumn(1).setPreferredWidth(40);
		readerJtable.getColumnModel().getColumn(2).setPreferredWidth(20);
		readerJtable.getColumnModel().getColumn(3).setPreferredWidth(20);

		ReaderTools readerTools = new ReaderTools();
		List<Reader> readerlist = readerTools.ReaderData();

		for (Reader temp : readerlist) {
			defaultModel.addRow(new Object[]{temp.getIdReader(), temp.getNameReader(), temp.getEmailReader(),
					temp.getPasswordReader()});
		}
		readerScrollPane.setViewportView(readerJtable);
	}

	private void do_search_reader() {

 		readerJtable = new JTable();
		readerJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		readerJtable.setRowHeight(54);

		defaultModel = (DefaultTableModel) readerJtable.getModel();
		defaultModel.setRowCount(0);
		defaultModel.setColumnIdentifiers(new Object[] { "Reader's ID", "Reader's Name", "Reader's Gender", "Password" });

		readerJtable.getTableHeader().setReorderingAllowed(false);
		readerJtable.setModel(defaultModel);

		readerJtable.getColumnModel().getColumn(0).setPreferredWidth(10);
		readerJtable.getColumnModel().getColumn(1).setPreferredWidth(40);
		readerJtable.getColumnModel().getColumn(2).setPreferredWidth(20);
		readerJtable.getColumnModel().getColumn(3).setPreferredWidth(20);

		ReaderTools readerTools = new ReaderTools();

		String keyword;
		if (searchtextField.getText() != null && !"".equals(searchtextField.getText())) {
			keyword = searchtextField.getText();
		} else {
			show_data();
			JOptionPane.showMessageDialog(this, "Please input reader's name", "", JOptionPane.WARNING_MESSAGE);
			return;
		}

		List<Reader> readerlist = readerTools.ReaderDataSearch(keyword);

		if (readerlist.size() == 0) {
			JOptionPane.showMessageDialog(this, "Reader not found ", "", JOptionPane.WARNING_MESSAGE);
		} else {
			for (Reader temp : readerlist) {
				defaultModel.addRow(new Object[]{temp.getIdReader(), temp.getNameReader(),
						temp.getEmailReader(), temp.getPasswordReader()});
			}
			readerScrollPane.setViewportView(readerJtable);
		}
	}

	//update reader's information
	private void update_Reader() {
		row = readerJtable.getSelectedRow();
		if (row == -1) {
			JOptionPane.showMessageDialog(this, "Select a reader", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		ReaderUpdateFrame reader_UpdateFrame = new ReaderUpdateFrame(allReadersFrame.this);
		reader_UpdateFrame.setVisible(true);
		CloseFrame();
	}

	//delete reader's information
	private void delete_data() {
		int row = readerJtable.getSelectedRow();
		if (row == -1) {
			JOptionPane.showMessageDialog(this, "Select a reader", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		ReaderTools readerTools = new ReaderTools();
		int i = readerTools.DeleteReader(Integer.parseInt(readerJtable.getValueAt(row, 0).toString()));
		if (i == 1) {
			JOptionPane.showMessageDialog(getContentPane(), "Deletion Succeeded", "", JOptionPane.WARNING_MESSAGE);
			this.show_data();
		} else {
			JOptionPane.showMessageDialog(getContentPane(), "Deletion failed", "", JOptionPane.WARNING_MESSAGE);
		}
	}

	private void addBlackuser() {

		BlacklistTools blacklistTools = new BlacklistTools();
		Blacklist blacklist = new Blacklist();

		int row = readerJtable.getSelectedRow();
		if (row == -1) {
			JOptionPane.showMessageDialog(this, "Select a reader", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		blacklist.setIdReaderBlack(Integer.parseInt(readerJtable.getValueAt(row, 0).toString()));
		blacklist.setNameReaderBlack(readerJtable.getValueAt(row, 1).toString());
		int i = blacklistTools.addBlackuser(blacklist);
		if (i == 1) {
			JOptionPane.showMessageDialog(getContentPane(), "Add to blacklist Successfully", "", JOptionPane.WARNING_MESSAGE);
			this.show_data();
		} else {
			JOptionPane.showMessageDialog(getContentPane(), "Add failed", "", JOptionPane.WARNING_MESSAGE);
		}
	}

	public void CloseFrame() {
		super.dispose();
	}
}
