package GuiLib;

import ModelLib.Reader;
import DBLib.ReaderTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ReaderRegisterFrame extends JFrame {

	private JPanel contentPane;

	private JTextField idReadertextField;
	private JTextField nameReadertextField;
	private JTextField emailtextField;
	private JTextField passwordtextField;

	private JLabel idReaderLabel;
	private JLabel nameReaderLabel;
	private JLabel emailLabel;
	private JLabel passwordLabel;

	private JButton insertButton;
	public ReaderRegisterFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton reader_Registerbutton = new JButton("Reader registration");
		reader_Registerbutton.addActionListener(e -> {
			ReaderRegisterFrame reader_RegisterFrame = new ReaderRegisterFrame();
			reader_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton book_Registerbutton = new JButton("Book registration");
		book_Registerbutton.addActionListener(e -> {
			BookRegisterFrame book_RegisterFrame = new BookRegisterFrame();
			book_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Readerbutton = new JButton("All readers");
		all_Readerbutton.addActionListener(e -> {
			allReadersFrame allReadersFrame = new allReadersFrame();
			allReadersFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Bookbutton = new JButton("All books");
		all_Bookbutton.addActionListener(e -> {
			allBooksFrame allBooksFrame = new allBooksFrame();
			allBooksFrame.setVisible(true);
			CloseFrame();
		});

		JButton checkReader_button = new JButton("Borrowing history");
		checkReader_button.addActionListener(e -> {
			BorrowingHistoryFrame borrowingHistoryFrame = new BorrowingHistoryFrame();
			borrowingHistoryFrame.setVisible(true);
			CloseFrame();
		});

		JButton log_out_Button = new JButton("Log out");
		log_out_Button.addActionListener(e -> {
			LogInFrame loginframe = new LogInFrame();
			loginframe.setVisible(true);
			CloseFrame();
		});

		JButton blacklistbutton = new JButton("Blacklist");
		blacklistbutton.addActionListener(e -> {
			blacklistFrame blacklistFrame = new blacklistFrame();
			blacklistFrame.setVisible(true);
			CloseFrame();
		});
		reader_Registerbutton.setBounds(20, 40, 170, 30);
		contentPane.add(reader_Registerbutton);
		book_Registerbutton.setBounds(20, 120, 170, 30);
		contentPane.add(book_Registerbutton);
		all_Readerbutton.setBounds(20, 200, 170, 30);
		contentPane.add(all_Readerbutton);
		all_Bookbutton.setBounds(20, 280, 170, 30);
		contentPane.add(all_Bookbutton);
		checkReader_button.setBounds(20, 360, 170, 30);
		contentPane.add(checkReader_button);
		blacklistbutton.setBounds(20, 440, 170, 30);
		contentPane.add(blacklistbutton);
		log_out_Button.setBounds(340, 500, 170, 40);
		contentPane.add(log_out_Button);


		idReaderLabel = new JLabel("Reader's ID");
		idReaderLabel.setBounds(300, 120, 100, 30);
		contentPane.add(idReaderLabel);

		idReadertextField = new JTextField();
		idReadertextField.setBounds(400, 120, 300, 30);
		contentPane.add(idReadertextField);
		idReadertextField.setColumns(10);

		nameReaderLabel = new JLabel("Reader's Name");
		nameReaderLabel.setBounds(300, 200, 100, 30);
		contentPane.add(nameReaderLabel);

		nameReadertextField = new JTextField();
		nameReadertextField.setBounds(400, 200, 300, 30);
		contentPane.add(nameReadertextField);
		nameReadertextField.setColumns(10);

		emailLabel = new JLabel("Reader's Email");
		emailLabel.setBounds(300, 280, 100, 30);
		contentPane.add(emailLabel);

		emailtextField = new JTextField();
		emailtextField.setBounds(400, 280, 300, 30);
		contentPane.add(emailtextField);
		emailtextField.setColumns(10);

		passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(300, 360, 100, 30);
		contentPane.add(passwordLabel);

		passwordtextField = new JTextField();
		passwordtextField.setBounds(400, 360, 300, 30);
		contentPane.add(passwordtextField);
		passwordtextField.setColumns(10);

		insertButton = new JButton("add a new reader");
		insertButton.addActionListener(e -> do_insertButton_AddReader());
		insertButton.setBounds(400, 50, 150, 40);
		contentPane.add(insertButton);
	}

	protected void do_insertButton_AddReader() {

		ReaderTools readerTools = new ReaderTools();
		Reader reader = new Reader();

		if (idReadertextField.getText() != null && !"".equals(idReadertextField.getText())
				&& nameReadertextField.getText() != null && !"".equals(nameReadertextField.getText())
				&& emailtextField.getText() != null && !"".equals(emailtextField.getText())
				&& passwordtextField.getText() != null && !"".equals(passwordtextField.getText())) {
			reader.setIdReader(Integer.parseInt(idReadertextField.getText()));
			reader.setNameReader(nameReadertextField.getText());
			reader.setEmailReader(emailtextField.getText());
			reader.setPasswordReader(passwordtextField.getText());
			int i = readerTools.AddReader(reader);
			if (i == 1) {
				JOptionPane.showMessageDialog(getContentPane(), "Add a reader successfully", "", JOptionPane.WARNING_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(getContentPane(), "Add failed, please try again ", "", JOptionPane.WARNING_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(getContentPane(), "Please input all the information", "", JOptionPane.WARNING_MESSAGE);
		}
	}

	public void CloseFrame() {
		super.dispose();
	}
}
