package GuiLib;

import DBLib.AuthorTools;
import DBLib.BookTools;
import DBLib.BorrowTools;
import DBLib.PublisherTools;
import ModelLib.Author;
import ModelLib.Book;
import ModelLib.Publisher;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

public class allBooksFrame extends JFrame implements ItemListener {

	private JPanel contentPane;

	private JScrollPane bookScrollPane;
	public JTable bookJtable;
	private DefaultTableModel defaultModel;
	// row: data table display
	public int row;

	private JButton updateButton;
	private JButton deleteButton;
	private JButton searchButton;

	//searchWay: define the searching methods: by name or by ID
	private String searchWay = "idBook";
	private JRadioButton nameBookRadioButton;
	private JRadioButton idBookRadioButton;
	private ButtonGroup group;

	private JTextField searchtextField;

	//Create a frame to show all books.
	public allBooksFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 900, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton reader_Registerbutton = new JButton("Reader registration");
		reader_Registerbutton.addActionListener(e -> {
			ReaderRegisterFrame reader_RegisterFrame = new ReaderRegisterFrame();
			reader_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton book_Registerbutton = new JButton("Book registration");
		book_Registerbutton.addActionListener(e -> {
			BookRegisterFrame book_RegisterFrame = new BookRegisterFrame();
			book_RegisterFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Readerbutton = new JButton("All readers");
		all_Readerbutton.addActionListener(e -> {
			allReadersFrame allReadersFrame = new allReadersFrame();
			allReadersFrame.setVisible(true);
			CloseFrame();
		});

		JButton all_Bookbutton = new JButton("All books");
		all_Bookbutton.addActionListener(e -> {
			allBooksFrame allBooksFrame = new allBooksFrame();
			allBooksFrame.setVisible(true);
			CloseFrame();
		});

		JButton checkReader_button = new JButton("Borrowing history");
		checkReader_button.addActionListener(e -> {
			BorrowingHistoryFrame borrowingHistoryFrame = new BorrowingHistoryFrame();
			borrowingHistoryFrame.setVisible(true);
			CloseFrame();
		});

		JButton log_out_Button = new JButton("Log out");
		log_out_Button.addActionListener(e -> {
			LogInFrame loginframe = new LogInFrame();
			loginframe.setVisible(true);
			CloseFrame();
		});

		JButton blacklistbutton = new JButton("Blacklist");
		blacklistbutton.addActionListener(e -> {
			blacklistFrame blacklistFrame = new blacklistFrame();
			blacklistFrame.setVisible(true);
			CloseFrame();
		});
		reader_Registerbutton.setBounds(20, 40, 170, 30);
		contentPane.add(reader_Registerbutton);
		book_Registerbutton.setBounds(20, 120, 170, 30);
		contentPane.add(book_Registerbutton);
		all_Readerbutton.setBounds(20, 200, 170, 30);
		contentPane.add(all_Readerbutton);
		all_Bookbutton.setBounds(20, 280, 170, 30);
		contentPane.add(all_Bookbutton);
		checkReader_button.setBounds(20, 360, 170, 30);
		contentPane.add(checkReader_button);
		blacklistbutton.setBounds(20, 440, 170, 30);
		contentPane.add(blacklistbutton);
		log_out_Button.setBounds(340, 500, 170, 40);
		contentPane.add(log_out_Button);


		bookScrollPane = new JScrollPane();
		bookScrollPane.setBounds(210, 110, 650, 300);
		contentPane.add(bookScrollPane);

		searchtextField = new JTextField();
		searchtextField.setBounds(270, 30, 300, 30);
		contentPane.add(searchtextField);
		searchtextField.setColumns(10);

		searchButton = new JButton("search");
		searchButton.addActionListener(e -> do_search_book());
		searchButton.setBounds(580, 30, 100, 30);
		contentPane.add(searchButton);

		idBookRadioButton = new JRadioButton("Search by book's ID ");
		idBookRadioButton.setBounds(300, 70, 200, 30);
		contentPane.add(idBookRadioButton);
		idBookRadioButton.setSelected(true);
		idBookRadioButton.addItemListener(this);
		idBookRadioButton.setContentAreaFilled(false);

		nameBookRadioButton = new JRadioButton("search by book's name");
		nameBookRadioButton.setBounds(500, 70, 200, 30);
		contentPane.add(nameBookRadioButton);
		nameBookRadioButton.addItemListener(this);
		nameBookRadioButton.setContentAreaFilled(false);

		group = new ButtonGroup();
		group.add(this.idBookRadioButton);
		group.add(this.nameBookRadioButton);

		updateButton = new JButton("update");
		updateButton.addActionListener(e -> update_book());
		updateButton.setBounds(350, 430, 100, 30);
		contentPane.add(updateButton);

		deleteButton = new JButton("delete");
		deleteButton.addActionListener(e -> delete_Book());
		deleteButton.setBounds(550, 430, 100, 30);
		contentPane.add(deleteButton);

		show_data();
	}

	// show all books' information in a table
	private void show_data() {

		bookJtable = new JTable();
		bookJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		bookJtable.setRowHeight(54);

		defaultModel = (DefaultTableModel) bookJtable.getModel();
		defaultModel.setRowCount(0);
		defaultModel.setColumnIdentifiers(new Object[] { "ID number", "name of book", "kind of Book", "author","au. birthyear","publisher", "pub. year","Whether in Stock" });
		bookJtable.getTableHeader().setReorderingAllowed(false);
		bookJtable.setModel(defaultModel);

		bookJtable.getColumnModel().getColumn(0).setPreferredWidth(15);
		bookJtable.getColumnModel().getColumn(1).setPreferredWidth(80);
		bookJtable.getColumnModel().getColumn(2).setPreferredWidth(50);
		bookJtable.getColumnModel().getColumn(3).setPreferredWidth(40);
		bookJtable.getColumnModel().getColumn(4).setPreferredWidth(40);
		bookJtable.getColumnModel().getColumn(5).setPreferredWidth(60);
		bookJtable.getColumnModel().getColumn(6).setPreferredWidth(40);
		bookJtable.getColumnModel().getColumn(7).setPreferredWidth(70);

		BookTools bookTools = new BookTools();
		AuthorTools authorTools = new AuthorTools();
		PublisherTools publisherTools = new PublisherTools();

		List<Book> booklist = bookTools.BookData();
		BorrowTools borrowtools = new BorrowTools();

		for (Book temp : booklist) {
			String whetherInStock;
			if (borrowtools.whetherInStock(temp.getIdBook())) {
				whetherInStock = "Book available";
			} else {
				whetherInStock = "Is borrowed";
			}
			List<Author> authorlist = authorTools.AuthorData(temp.getNameAuthor());
			Publisher publisher = publisherTools.PublisherData((temp.getNamePublisher()));
			defaultModel.addRow(new Object[]{temp.getIdBook(), temp.getNameBook(), temp.getKind(),
					temp.getNameAuthor(),authorlist.get(0).getBirthyear(), temp.getNamePublisher(),publisher.getPubyear(), whetherInStock});
		}
		bookScrollPane.setViewportView(bookJtable);
	}

	//Searching by book's ID or by book's name
	public void itemStateChanged(ItemEvent e) {
		if (idBookRadioButton.isSelected()) {
			this.searchWay = "idBook";
		}
		if (nameBookRadioButton.isSelected()) {
			this.searchWay = "nameBook";
		}
	}


	private void do_search_book() {
		//search by name of book
		if ("nameBook".equals(searchWay)) {

			bookJtable = new JTable();
			bookJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
			bookJtable.setRowHeight(54);

			defaultModel = (DefaultTableModel) bookJtable.getModel();
			defaultModel.setRowCount(0);
			defaultModel.setColumnIdentifiers(new Object[] { "ID number", "name of book", "kind of Book", "author","au. birthyear","publisher", "pub. year","Whether in Stock" });
			bookJtable.getTableHeader().setReorderingAllowed(false);
			bookJtable.setModel(defaultModel);

			bookJtable.getColumnModel().getColumn(0).setPreferredWidth(15);
			bookJtable.getColumnModel().getColumn(1).setPreferredWidth(80);
			bookJtable.getColumnModel().getColumn(2).setPreferredWidth(50);
			bookJtable.getColumnModel().getColumn(3).setPreferredWidth(40);
			bookJtable.getColumnModel().getColumn(4).setPreferredWidth(40);
			bookJtable.getColumnModel().getColumn(5).setPreferredWidth(60);
			bookJtable.getColumnModel().getColumn(6).setPreferredWidth(40);
			bookJtable.getColumnModel().getColumn(7).setPreferredWidth(70);

			BookTools booktools = new BookTools();
			AuthorTools authorTools = new AuthorTools();
			PublisherTools publisherTools = new PublisherTools();

			BorrowTools borrowtools = new BorrowTools();

			//keyword searching by name
			String keyword;
			if (searchtextField.getText() != null && !"".equals(searchtextField.getText())) {
				keyword = searchtextField.getText();
			} else {
				show_data();
				JOptionPane.showMessageDialog(this, "Please input book's name", "", JOptionPane.WARNING_MESSAGE);
				return;
			}

			List<Book> booklist = booktools.BookData(keyword);

			if (booklist.size() == 0) {
				JOptionPane.showMessageDialog(this, "Book not found", "", JOptionPane.WARNING_MESSAGE);
				return;
			} else {
				for (Book temp : booklist) {
					String whetherInStock;
					if (borrowtools.whetherInStock(temp.getIdBook())) {
						whetherInStock = "Book available";
					} else {
						whetherInStock = "Is borrowed";
					}
					List<Author> authorlist = authorTools.AuthorData(temp.getNameAuthor());
					Publisher publisher = publisherTools.PublisherData((temp.getNamePublisher()));
					defaultModel.addRow(new Object[]{temp.getIdBook(), temp.getNameBook(), temp.getKind(),
							temp.getNameAuthor(),authorlist.get(0).getBirthyear(), temp.getNamePublisher(),publisher.getPubyear(), whetherInStock});
				}
				bookScrollPane.setViewportView(bookJtable);
			}
		}

		//search by book's ID
		if ("idBook".equals(searchWay)) {

			bookJtable = new JTable();
			bookJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
			bookJtable.setRowHeight(54);

			defaultModel = (DefaultTableModel) bookJtable.getModel();
			defaultModel.setRowCount(0);
			defaultModel.setColumnIdentifiers(new Object[] { "ID number", "name of book", "kind of Book", "author","au. birthyear","publisher", "pub. year","Whether in Stock" });
			bookJtable.getTableHeader().setReorderingAllowed(false);
			bookJtable.setModel(defaultModel);

			bookJtable.getColumnModel().getColumn(0).setPreferredWidth(15);
			bookJtable.getColumnModel().getColumn(1).setPreferredWidth(80);
			bookJtable.getColumnModel().getColumn(2).setPreferredWidth(50);
			bookJtable.getColumnModel().getColumn(3).setPreferredWidth(40);
			bookJtable.getColumnModel().getColumn(4).setPreferredWidth(40);
			bookJtable.getColumnModel().getColumn(5).setPreferredWidth(60);
			bookJtable.getColumnModel().getColumn(6).setPreferredWidth(40);
			bookJtable.getColumnModel().getColumn(7).setPreferredWidth(70);

			BorrowTools borrowtools = new BorrowTools();
			AuthorTools authorTools = new AuthorTools();
			PublisherTools publisherTools = new PublisherTools();

			int keyword;
			if (searchtextField.getText() != null && !"".equals(searchtextField.getText())) {
				keyword = Integer.parseInt(searchtextField.getText());
				//System.out.println(keyword);
			} else {
				show_data();
				JOptionPane.showMessageDialog(this, "Please input book's ID", "", JOptionPane.WARNING_MESSAGE);
				return;
			}

			Book book = BookTools.Search_Book(keyword);

			if (book == null) {
				JOptionPane.showMessageDialog(this, "book not found", "", JOptionPane.WARNING_MESSAGE);
			} else {
				String whetherInStock;
				if (borrowtools.whetherInStock(book.getIdBook())) {
					whetherInStock = "Book available";
				} else {
					whetherInStock = "Is borrowed";
				}
				List<Author> authorlist = authorTools.AuthorData(book.getNameAuthor());
				Publisher publisher = publisherTools.PublisherData((book.getNamePublisher()));
				defaultModel.addRow(new Object[]{book.getIdBook(), book.getNameBook(), book.getKind(),
						book.getNameAuthor(),authorlist.get(0).getBirthyear(), book.getNamePublisher(),publisher.getPubyear(), whetherInStock});

				bookScrollPane.setViewportView(bookJtable);
			}
		}
	}

	//update a book
	private void update_book() {
		row = bookJtable.getSelectedRow();
		if (row == -1) {
			JOptionPane.showMessageDialog(this, "Select a book", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		BookUpdateFrame book_UpdateFrame = new BookUpdateFrame(allBooksFrame.this);
		book_UpdateFrame.setVisible(true);
		CloseFrame();
	}

	//delete a book
	private void delete_Book() {
		row = bookJtable.getSelectedRow();
		if (row == -1) {
			JOptionPane.showMessageDialog(this, "Choose a book", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		BookTools bookTools = new BookTools();
		int i = bookTools.DeleteBook(Integer.parseInt(bookJtable.getValueAt(row, 0).toString()));
		if (i == 1) {
			JOptionPane.showMessageDialog(getContentPane(), "Deletion succeeded", "", JOptionPane.WARNING_MESSAGE);
			this.show_data();
		} else {
			JOptionPane.showMessageDialog(getContentPane(), "Deletion failed", "", JOptionPane.WARNING_MESSAGE);
		}
	}

	public void CloseFrame() {
		super.dispose();
	}

}
