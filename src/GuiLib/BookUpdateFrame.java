package GuiLib;

import ModelLib.Author;
import ModelLib.Book;
import ModelLib.Publisher;
import DBLib.AuthorTools;
import DBLib.BookTools;
import DBLib.PublisherTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class BookUpdateFrame extends JFrame {
	private JPanel contentPane;
	
	private JTextField nameBooktextField;
	private JTextField kindtextField;
	private JTextField authortextField;
	private JTextField publishertextField;	
	private JTextField authorBirthdayField;
	private JTextField publisherPubdateField;

	private JLabel idBookLabel;
	private JLabel idBook_showLabel;
	private JLabel nameBookLabel;
	private JLabel kindLabel;
	private JLabel authorLabel;
	private JLabel publisherLabel;
	private JLabel authorBirthdayLabel;
	private JLabel publisherPubdateLabel;
	private JButton updateButton;

	public BookUpdateFrame(allBooksFrame allBooksFrame) {
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		idBookLabel = new JLabel("Book's ID");
		idBookLabel.setBounds(100, 100, 100, 30);
		contentPane.add(idBookLabel);

		idBook_showLabel = new JLabel();
		idBook_showLabel.setBounds(220, 100, 300, 30);
		contentPane.add(idBook_showLabel);
		idBook_showLabel.setText(allBooksFrame.bookJtable.getValueAt(allBooksFrame.row, 0).toString());

		nameBookLabel = new JLabel("Book's Name");
		nameBookLabel.setBounds(100, 150, 100, 30);
		contentPane.add(nameBookLabel);

		nameBooktextField = new JTextField();
		nameBooktextField.setBounds(220,150,300,30);
		contentPane.add(nameBooktextField);
		nameBooktextField.setColumns(10);
		nameBooktextField.setText(allBooksFrame.bookJtable.getValueAt(allBooksFrame.row, 1).toString());

		kindLabel = new JLabel("Book's kind");
		kindLabel.setBounds(100, 200, 100, 30);
		contentPane.add(kindLabel);

		kindtextField = new JTextField();
		kindtextField.setBounds(220,200,300,30);
		contentPane.add(kindtextField);
		kindtextField.setColumns(10);
		kindtextField.setText(allBooksFrame.bookJtable.getValueAt(allBooksFrame.row, 2).toString());

		authorLabel = new JLabel("Author");
		authorLabel.setBounds(100, 250, 100, 30);
		contentPane.add(authorLabel);

		authortextField = new JTextField();
		authortextField.setBounds(220, 250, 300, 30);
		contentPane.add(authortextField);
		authortextField.setColumns(10);
		authortextField.setText(allBooksFrame.bookJtable.getValueAt(allBooksFrame.row, 3).toString());

		authorBirthdayLabel = new JLabel("Author's Birthday");
		authorBirthdayLabel.setBounds(100, 300, 100, 30);
		contentPane.add(authorBirthdayLabel);

		authorBirthdayField = new JTextField();
		authorBirthdayField.setBounds(220, 300, 300, 30);
		contentPane.add(authorBirthdayField);
		authorBirthdayField.setColumns(10);
		authorBirthdayField.setText(allBooksFrame.bookJtable.getValueAt(allBooksFrame.row, 4).toString());

		publisherLabel = new JLabel("Publisher");
		publisherLabel.setBounds(100, 350, 100, 30);
		contentPane.add(publisherLabel);

		publishertextField = new JTextField();
		publishertextField.setBounds(220, 350, 300, 30);
		contentPane.add(publishertextField);
		publishertextField.setColumns(10);
		publishertextField.setText(allBooksFrame.bookJtable.getValueAt(allBooksFrame.row, 5).toString());

		publisherPubdateLabel = new JLabel("Publish date");
		publisherPubdateLabel.setBounds(100, 400, 100, 30);
		contentPane.add(publisherPubdateLabel);

		publisherPubdateField = new JTextField();
		publisherPubdateField.setBounds(220, 400, 300, 30);
		contentPane.add(publisherPubdateField);
		publisherPubdateField.setColumns(10);
		publisherPubdateField.setText(allBooksFrame.bookJtable.getValueAt(allBooksFrame.row, 6).toString());

		updateButton = new JButton("update");
		updateButton.addActionListener(e -> do_updateButton_UpdateBook());
		updateButton.setBounds(200, 50, 200, 30);
		contentPane.add(updateButton);

	}

	protected void do_updateButton_UpdateBook() {

		BookTools bookTools = new BookTools();
		Book book = new Book();
		
		Author author = new Author();
		AuthorTools authorTools = new AuthorTools();
		
		Publisher publisher= new Publisher();
		PublisherTools publisherTools = new PublisherTools();
		
		if ( idBook_showLabel.getText() != null && !"".equals(idBook_showLabel.getText()) 
				&& nameBooktextField.getText() != null && !"".equals(nameBooktextField.getText())
				&& kindtextField.getText() != null && !"".equals(kindtextField.getText())
				&& authortextField.getText() != null && !"".equals(authortextField.getText())
				&& publishertextField.getText() != null && !"".equals(publishertextField.getText())
				&& authorBirthdayField.getText()!= null && !"".equals(authorBirthdayField.getText())
				&& publisherPubdateField.getText() != null && !"".equals(publisherPubdateField.getText())) {
			book.setIdBook(Integer.parseInt(idBook_showLabel.getText()));
			book.setNameBook(nameBooktextField.getText());
			book.setKind(kindtextField.getText());
			book.setNameAuthor(authortextField.getText());
			book.setNamePublisher(publishertextField.getText());
			author.setNameAuthor(authortextField.getText());
			author.setBirthyear(Integer.parseInt(authorBirthdayField.getText()));
			publisher.setNamePublisher(publishertextField.getText());
			publisher.setPubyear(Integer.parseInt(publisherPubdateField.getText()));
			
			// try updating
			publisherTools.UpdatePublisher(publisher);
			publisherTools.AddPublisher(publisher);				
			authorTools.UpdateAuthor(author);
			authorTools.AddAuthor(author);
			
			int rs = bookTools.UpdateBook(book);
			if ( rs == 1) {
	            JOptionPane.showMessageDialog(getContentPane(), "Update successfully", "", JOptionPane.WARNING_MESSAGE);
			} else {
	            JOptionPane.showMessageDialog(getContentPane(), "Update failed, please try again", "", JOptionPane.WARNING_MESSAGE);
			}

		} else {
            JOptionPane.showMessageDialog(getContentPane(), "Please input all the book's information", "", JOptionPane.WARNING_MESSAGE);
		}
		allBooksFrame allBooksFrame = new allBooksFrame();
		allBooksFrame.setVisible(true);
		CloseFrame();
	}
	public void CloseFrame(){
	    super.dispose();
	} 
}
