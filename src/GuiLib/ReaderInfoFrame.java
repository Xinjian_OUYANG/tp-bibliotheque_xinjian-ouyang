package GuiLib;

import ModelLib.Book;
import ModelLib.Reader;
import DBLib.BorrowTools;
import DBLib.ReaderTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class ReaderInfoFrame extends JFrame {
	private JPanel contentPane;

	private JScrollPane bookScrollPane;
	private JTable bookJtable;
	private DefaultTableModel defaultModel;
	public static int row;

	private JLabel idReaderLabel;
	private JLabel nameReaderLabel;
	private JLabel emailLabel;
	private JLabel showidReaderLabel;
	private JLabel showNameReaderLabel;
	private JLabel showemailLabel;

	private JButton return_BookButton;

	public ReaderInfoFrame() {

		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton borrow_Button = new JButton("Borrow a book");
		borrow_Button.addActionListener(e -> {
			BookBorrowFrame _BookBorrowFrame = new BookBorrowFrame();
			_BookBorrowFrame.setVisible(true);
			CloseFrame();
		});

		JButton self_info_Button = new JButton("My borrowing information");
		self_info_Button.addActionListener(e -> {

			ReaderInfoFrame _ReaderInfoFrame = new ReaderInfoFrame();
			_ReaderInfoFrame.setVisible(true);
			CloseFrame();
		});

		JButton log_out_Button = new JButton("Log out");
		log_out_Button.addActionListener(e -> {
			LogInFrame loginframe = new LogInFrame();
			loginframe.setVisible(true);
			CloseFrame();
		});

		borrow_Button.setBounds(140, 20, 200, 30);
		contentPane.add(borrow_Button);
		self_info_Button.setBounds(540, 20, 200, 30);
		contentPane.add(self_info_Button);
		log_out_Button.setBounds(340, 500, 120, 40);
		contentPane.add(log_out_Button);

		idReaderLabel = new JLabel("Reader's ID");
		idReaderLabel.setBounds(270, 70, 150, 20);
		contentPane.add(idReaderLabel);

		showidReaderLabel = new JLabel();
		showidReaderLabel.setBounds(420, 70, 200, 20);
		contentPane.add(showidReaderLabel);
		showidReaderLabel.setText(String.valueOf(LogInFrame.idReader));

		nameReaderLabel = new JLabel("Reader's Name");
		nameReaderLabel.setBounds(270, 90, 150, 20);
		contentPane.add(nameReaderLabel);

		showNameReaderLabel = new JLabel("");
		showNameReaderLabel.setBounds(420, 90, 200, 20);
		contentPane.add(showNameReaderLabel);

		emailLabel = new JLabel("Reader's email");
		emailLabel.setBounds(270, 110, 150, 20);
		contentPane.add(emailLabel);

		showemailLabel = new JLabel("");
		showemailLabel.setBounds(420, 110, 200, 20);
		contentPane.add(showemailLabel);

		bookScrollPane = new JScrollPane(bookJtable);
		bookScrollPane.setBounds(50, 150, 700, 330);
		contentPane.add(bookScrollPane);

		return_BookButton = new JButton("Return selected book");
		return_BookButton.addActionListener(e -> do_return_book());
		return_BookButton.setBounds(55, 110, 200, 35);
		contentPane.add(return_BookButton);

		show_data();
	}

	private void show_data() {

		bookJtable = new JTable();
		bookJtable.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		bookJtable.setRowHeight(54);

		defaultModel = (DefaultTableModel) bookJtable.getModel();
		defaultModel.setRowCount(0);
		defaultModel.setColumnIdentifiers(new Object[] { "Book's ID", "Book's Name", "Book's Kind", "Author", "Publisher" });

		bookJtable.getTableHeader().setReorderingAllowed(false);
		bookJtable.setModel(defaultModel);

		bookJtable.getColumnModel().getColumn(0).setPreferredWidth(10);
		bookJtable.getColumnModel().getColumn(1).setPreferredWidth(80);
		bookJtable.getColumnModel().getColumn(2).setPreferredWidth(20);
		bookJtable.getColumnModel().getColumn(3).setPreferredWidth(20);
		bookJtable.getColumnModel().getColumn(4).setPreferredWidth(10);

		//reader's information
		ReaderTools readerTools = new ReaderTools();
		Reader reader = new Reader();
		BorrowTools borrowtools = new BorrowTools();

		if (showidReaderLabel.getText() != null && !"".equals(showidReaderLabel.getText())) {
			reader.setIdReader(Integer.parseInt(showidReaderLabel.getText()));
		} else {
			JOptionPane.showMessageDialog(this, "Please input reader's ID", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		List<Reader> readerlist = readerTools.ReaderData(reader.getIdReader());
		List<Book> booklist = borrowtools.BookData(reader.getIdReader());

		// Check the idReader
		if (readerlist.size() == 0) {
			JOptionPane.showMessageDialog(this, "Wrong ID number, Please input correct reader's ID", "", JOptionPane.WARNING_MESSAGE);
			return;
		} else {
			for (Reader temp : readerlist) {
				showNameReaderLabel.setText(temp.getNameReader());
				showemailLabel.setText(temp.getEmailReader());
			}
			for (Book temp : booklist) {
				defaultModel.addRow(new Object[]{temp.getIdBook(), temp.getNameBook(),
						temp.getKind(), temp.getNameAuthor(), temp.getNameAuthor()});
			}
		}
		bookScrollPane.setViewportView(bookJtable);
	}

	private void do_return_book() {

		row = bookJtable.getSelectedRow();
		if (row == -1) {
			JOptionPane.showMessageDialog(this, "Select a book", "", JOptionPane.WARNING_MESSAGE);
			return;
		}
		BorrowTools borrowtools = new BorrowTools();
		int idbook = Integer.parseInt(bookJtable.getValueAt(row, 0).toString());
		int i = borrowtools.ReturnBook(idbook);
		int j = borrowtools.ReturnHis(idbook);
		System.out.println("return"+i+"+"+j);
		if (i == 1 && j != 0) {
			JOptionPane.showMessageDialog(this, "Return Successfully", "", JOptionPane.WARNING_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(this, "Return failed, please try again", "", JOptionPane.WARNING_MESSAGE);
		}
		show_data();
	}

	public void CloseFrame() {
		super.dispose();
	}
}
